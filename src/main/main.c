#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LPC17xx.h"
#include "comm.h"
#include "monitor.h"
#include "lpc17xx_gpio.h"
#include "uart.h"
#include "lwip/mem.h"
#include "lwip/memp.h"
#include "lwip/pbuf.h"
#include "lwip/udp.h"
#include "lwip/tcp.h"
#include "lwip/dhcp.h"
#include "lwip/tcp_impl.h"
#include "lwip/opt.h"
#include "netif/ethernetif.h"
#include "netif/etharp.h"
#include "app/system_init.h"
#include "delay.h"
#include "uart.h"
#include "cypher.h"
#include "rtc_internal.h"
#include "diskio.h"
#include "can_driver.h"
#include "ili9340.h"
#include "xpt2046.h"
#include "lpc17xx_i2c.h"


//#include "app/eth_func.h"
//#include "app/sys_func.h"

/*******************************************************************************************/
/*				Global variables
********************************************************************************************/
uint32_t enc_deg_per_rot = 0, enc_no_of_turns = 0;
uint32_t can_counter = 0;
float enc_slope = 0, enc_offset = 0;
float home_offset = 0;
float home_set = 2500;
uint32_t calibr_init_sens_val = 0, calibr_final_sens_val = 0;
float calibr_init_user_val = 0, calibr_final_user_val = 0;
command_t *command_ptr;
com_states_t com_state;
struct com_state com_state_handler;
CanStruct_t can_arry[NO_OF_PARAMETERS];
uint32_t can_udp_buf[CAN_UDP_TX_SIZE];
uint32_t curr_pos_count = 0;
uint32_t last_pos_count = 0;
uint8_t touch_indicator = 1;
static float scale_factor_enc;
struct ip_addr ip_computer;

struct pbuf *udp_buf, *tcp_buf;

uint32_t can_config_len = 0;
extern int16_t xpt2046_x,xpt2046_y;
extern const unsigned char menu_button_150_50[];
extern const unsigned char menu_button_60_30[];
extern const unsigned char menu_button_pressed_150_50[];
extern const unsigned char menu_button_100_50[];
extern const unsigned char josts_logo [];

uint8_t i2c_rx_buffer[32];
uint8_t i2c_tx_buffer[32];
uint32_t password;

uint32_t units = 0;
float number;
uint8_t tcp_snd_buf[20] = "SENT ON TCP >>>>>>>>";

buffer_t buffer_struct;
sd_log_t sector_log;

I2C_M_SETUP_Type i2c_trans_cfg;

/*****************************************************************************************
 * 	SysTick Handler.
 *	timer_tick decremented in the handler is used to get a delay of required time
 *	refer: delay.c
 *****************************************************************************************/
void SysTick_Handler(void)
{
	systick_counter++;
//	if(!led_blink_cnt--)
//	{
//		LPC_GPIO0->FIOPIN ^= (1 << 10);		// Toggles the LED pin on the LPCxpresso board
//		led_blink_cnt = 1000;				// Board running indication
//	}

//	disk_timerproc();

	if(timer_tick > 0)						// Refer the delay.c file for use of timer_tick
    {
		--timer_tick;
    }
}

/*****************************************************************************************
 * 	sys_now(void)
 * 	This function is used by the LWIP sys_check_timeouts()
 * 	Returns the sysTick count and used by the sys_check_timeouts()
 * 	to compare the time past since last timeout.
 ****************************************************************************************/
u32_t sys_now(void)
{
	return systick_counter;
}


void can_change_baud()
{
	CanMessage can_msg;
	can_msg.id = 0x600 + 0x20;
	can_msg.length = 5;
	can_msg.data[0] = 0x2f;
	can_msg.data[1] = 0x01;
	can_msg.data[2] = 0x30;
	can_msg.data[3] = 0x00;
	can_msg.data[4] = 0x00;
	can_msg.data[5] = 0x00;
	can_msg.data[6] = 0x00;
	can_msg.data[7] = 0x00;
	can2_send(&can_msg, 0);
	delay_ms(10);
	can2_receive(&can_msg);
}

void can_save()
{
	CanMessage can_msg;
	can_msg.id = 0x600 + 0x20;
		can_msg.length = 8;
		can_msg.data[0] = 0x23;
		can_msg.data[1] = 0x00;
		can_msg.data[2] = 0x23;
		can_msg.data[3] = 0x00;
		can_msg.data[4] = 0x55;
		can_msg.data[5] = 0xAA;
		can_msg.data[6] = 0xAA;
		can_msg.data[7] = 0x55;
		can2_send(&can_msg, 0);
		delay_ms(10);
		can2_receive(&can_msg);
}

/****************************************************************************************
 * Function to initialise the controller
 * Set directions for IO pins
 * Set IO pin initial conditionsfconfig
 *
 * Configure internal registers.
 * Configure initial parameters
 ***************************************************************************************/
void config_controller()
{
	CanMessage can_msg;

	SysTick_Config(100000000 / TIMER_TICK_HZ);	// Controller clock is @ 100MHz

	LPC_GPIO0->FIODIR |= (1 << 10); 			// Pin P0.10 connected to onboard LED
												// direction set as output
	LPC_GPIO2->FIODIR |= (1 << 0);
	LPC_GPIO0->FIODIR &= ~(1 << 23);	        // DI0 input

	LPC_GPIO1->FIODIR |= (1 << 25); 			//DO0 output
	LPC_GPIO1->FIOCLR = (1 << 25);

#ifdef USE_BUTTONS
	LPC_GPIO4->FIODIR &= ~(Button_1 | Button_2);	// All buttons set as inputs
	LPC_GPIO0->FIODIR &= ~(Button_3);
	LPC_GPIO1->FIODIR &= ~(Button_4);

	FIO_SetMask(4, 0xCFFFFFFF, 1);
	FIO_SetMask(0, 0xFFFFF7FF, 1);
	FIO_SetMask(1, 0xFFDFFFFF, 1);
#endif

	comm_init();
	uart_init();
//	lwip_init();
	rtc_init();

	ili9340_init();
	ili9340_setRotation(3);

	lcd_disp_logo();
  //ili9340_draw_bitmap(120, 40, josts_logo, ILI9340_ORANGE);
//	disk_initialize(0);		// initializing uSD card

	xpt2046_init();
	xpt2046_get_data();

//	can2_init(3);			 // preinitialized for IFM RM9000 encoder
//	delay_ms(500);
//	reset_encoder();
//	can_change_baud();
//	delay_ms(500);
//	can_save();
//	delay_ms(500);
	can2_init(0);
	delay_ms(500);
	reset_encoder();
	delay_ms(500);

	can_change_guard_time();
	can_change_life_time();

	can_save();

	send_can_msg();			// turn
	can_operational_mode();


	can_msg.id = 0x620;
	can_msg.length = 6;
	can_msg.data[0] = 0x4B;
	can_msg.data[1] = 0x0C;
	can_msg.data[2] = 0x10;
	can_msg.data[3] = 0x00;
	can_msg.data[4] = 0x00;
	can_msg.data[5] = 0x00;
	can_msg.data[6] = 0x00;
	can_msg.data[7] = 0x00;
	can2_send(&can_msg, 0);
	delay_ms(500);
	can2_receive(&can_msg);


	LPC_SSP1->CPSR = 4;
	home_offset = 0;
}

void can_change_guard_time()
{
	CanMessage can_msg;
	can_msg.id = 0x620;
	can_msg.length = 6;
	can_msg.data[0] = 0x2B;
	can_msg.data[1] = 0x0C;
	can_msg.data[2] = 0x10;
	can_msg.data[3] = 0x00;
	can_msg.data[4] = 0x64;
	can_msg.data[5] = 0x00;
	can_msg.data[6] = 0x00;
	can_msg.data[7] = 0x00;
	can2_send(&can_msg, 0);
}

void can_change_life_time()
{
	CanMessage can_msg;
	can_msg.id = 0x620;
	can_msg.length = 5;
	can_msg.data[0] = 0x2F;
	can_msg.data[1] = 0x0D;
	can_msg.data[2] = 0x10;
	can_msg.data[3] = 0x00;
	can_msg.data[4] = 0x0A;
	can_msg.data[5] = 0x00;
	can_msg.data[6] = 0x00;
	can_msg.data[7] = 0x00;
	can2_send(&can_msg, 0);
}

void send_can_msg()
{
	CanMessage can_msg;
	can_msg.id = 0x620;
	can_msg.length = 6;
	can_msg.data[0] = 0x2B;
	can_msg.data[1] = 0x00;
	can_msg.data[2] = 0x62;
	can_msg.data[3] = 0x00;
	can_msg.data[4] = 0x00;
	can_msg.data[5] = 0x00;
	can_msg.data[6] = 0x00;
	can_msg.data[7] = 0x00;
	can2_send(&can_msg, 0);
	delay_ms(400);
	can2_receive(&can_msg);
}

void can_frame_tx_buf(uint32_t *buf_ptr, uint16_t data_len)
{
	uint32_t temp_holder;
	for(uint16_t i = 0; i < (data_len); i++)
	{
		temp_holder = can_arry[i].index;
		temp_holder = (temp_holder << 16);
		temp_holder |= can_arry[i].subindex;
		*(buf_ptr++) = temp_holder;
		temp_holder = 0;
		for(uint8_t z = 0; z < 4; z++)
		{
			//can_arry[i].data[7] = 1;
			temp_holder |= can_arry[i].data[z];
			if(z < 3)
			temp_holder = (temp_holder << 8);
		}
		*(buf_ptr++) = temp_holder;
	}
}

void Delay (uint32_t dlyTicks)
{
	uint32_t curTicks;
	curTicks = systick_counter;
	while ((systick_counter - curTicks) < dlyTicks);
}

void i2c_init(I2C_M_SETUP_Type *i2c_trans_cfg, uint32_t clock_speed)
{
	LPC_GPIO0->FIODIR |= (1 << 27) | (1 << 28);
	LPC_PINCON->PINSEL1 |= (1 << 22)| (1 << 24);	// Ser the pins in SDA and SCL
	LPC_SC->PCONP |= (1 << 7);						// power up the I2C0 peripheral

	LPC_I2C0->I2CONSET |= (1 << 6);					// Enable I2C interface

	I2C_Init(LPC_I2C0, clock_speed);
	i2c_trans_cfg->sl_addr7bit = SLAVE_ADDR;
	i2c_trans_cfg->tx_data = i2c_tx_buffer;
	i2c_trans_cfg->tx_length = 1;
	i2c_trans_cfg->tx_count = 0;
	i2c_trans_cfg->rx_data = i2c_rx_buffer;
	i2c_trans_cfg->rx_length = 0;
	i2c_trans_cfg->rx_count = 0;
	i2c_trans_cfg->retransmissions_max = 10;
	i2c_trans_cfg->retransmissions_count = 0;
	i2c_trans_cfg->status = 0;

	I2C_Cmd(LPC_I2C0, ENABLE);
}


/************************************************************************************************************************
 * Block will read the last locations address written to SD card
 * SD_mem_ptr will hold address last written to.
 * Temporary settings:
 * 				For Josts, save frame is 14512 Bytes long. Sector last uasble address is sector_start_addr + 490)
 * 				For A1-FEnce.....
 ***********************************************************************************************************************/
void SD_write_sensor_data(uint8_t sensor_num)
{
	uint32_t sector, sector_start_addr, sector_last_addr, epoch_time;
	uint16_t index = 0;
	/* Read the Metadata for the address to save the data @	*/
	disk_read_block(0, SD_read_buffer, METADATA_SECTOR_1, 1);
	memcpy(&SD_mem_ptr, SD_read_buffer, sizeof(SD_mem_ptr));

	/* Find out the sector of operation		*/
	sector = (SD_mem_ptr / SECTOR_SIZE);		// Sector 0 is reserved for METADATA
	sector_start_addr = SECTOR_SIZE * sector;
	sector_last_addr = sector_start_addr + 490;	// last contiguously usable address in one sector

	/* Read the sector and update the information at the desired location 	*/
	disk_read_block(0, SD_write_buffer, sector , 1);

	index = SD_mem_ptr % SECTOR_SIZE;
	epoch_time = rtc_get_now_epoch();

//temp_holder_32 = can_arry[sensor_num].data[3];
//temp_holder_32 = can_arry[sensor_num].data[2] + (temp_holder_32 << 8);
//temp_holder_32 = can_arry[sensor_num].data[1] + (temp_holder_32 << 8);
//temp_holder_32 = can_arry[sensor_num].data[0] + (temp_holder_32 << 8);
//
//temp_holder_32 = temp_holder_32 + (enc_no_of_turns * enc_deg_per_rot);


	SD_write_buffer[index++] = (can_arry[sensor_num].id);
	SD_write_buffer[index++] = (can_arry[sensor_num].id >> 8);
	SD_write_buffer[index++] = (can_arry[sensor_num].index);
	SD_write_buffer[index++] = (can_arry[sensor_num].index >> 8);
	SD_write_buffer[index++] = (can_arry[sensor_num].subindex);
	SD_write_buffer[index++] = (can_arry[sensor_num].subindex >> 8);
	SD_write_buffer[index++] = can_arry[sensor_num].data[0];
	SD_write_buffer[index++] = can_arry[sensor_num].data[1];
	SD_write_buffer[index++] = can_arry[sensor_num].data[2];
	SD_write_buffer[index++] = can_arry[sensor_num].data[3];
//	SD_write_buffer[index++] = temp_holder_32;
//	SD_write_buffer[index++] = temp_holder_32 >> 8;
//	SD_write_buffer[index++] = temp_holder_32 >> 16;
//	SD_write_buffer[index++] = temp_holder_32 >> 24;
	SD_write_buffer[index++] = epoch_time;
	SD_write_buffer[index++] = (epoch_time >> 8);
	SD_write_buffer[index++] = (epoch_time >> 16);
	SD_write_buffer[index++] = (epoch_time >> 24);
	/*	Update the data into the SD card	*/
	disk_write_block(0, SD_write_buffer, sector, 1);
	/* update SD_mem_ptr with the next location to save data.		*/
	/* TODO:: Currently the data written to SD for each sensor is 9 bytes. below sentenced needs to be better implemented to accommodate variable length data		*/
	SD_mem_ptr += 14;
	/* Each sector is limited to store only  bytes as our data is of 12 bytes and we don not wish to have a frame of data spread over two sectors		*/
	if(SD_mem_ptr > sector_last_addr)
	{
		sector += 1;
		sector_start_addr = SECTOR_SIZE * sector;
		SD_mem_ptr = sector_start_addr;
	}
	/*  Update the METADATA sector in SD card with the next location of ptr		*/
	disk_read_block(0, SD_read_buffer, METADATA_SECTOR_1, 1);

	memcpy(SD_read_buffer, &SD_mem_ptr, sizeof(SD_mem_ptr));

	disk_write_block(0, SD_read_buffer, METADATA_SECTOR_1, 1);
}

void can_operational_mode()
{
	CanMessage can_msg;
	can_msg.id = 0x00;
	can_msg.length = 2;
	can_msg.data[0] = 0x01;
	can_msg.data[1] = 0x20;			// Node ID specific to the default ID of the IFM RM9000

	can2_send(&can_msg, 0);
}

void can_preoperational_mode()
{
	CanMessage can_msg;
	can_msg.id = 0x00;
	can_msg.length = 2;
	can_msg.data[0] = 0x80;
	can_msg.data[1] = 0x20;			// Node ID specific to the default ID of the IFM RM9000

	can2_send(&can_msg, 0);
}

void reset_encoder()
{
	CanMessage can_msg;
	can_msg.id = 0x00;
	can_msg.length = 2;
	can_msg.data[0] = 0x81;
	can_msg.data[1] = 0x20;			// Node ID specific to the default ID of the IFM RM9000

	can2_send(&can_msg, 0);
}

void can_send_command(uint32_t save_indicator)
{
	CanMessage can_msg;
	uint32_t routine_strt_time = systick_counter;
	volatile uint32_t tmp = NO_MESSAGE_RECEIVED;
	uint8_t no_of_can_sensors = param_poll_num;

	delay_ms(30);

	can_counter++;
	if(can_counter == 500)
	{
//		reset_encoder();
////		delay_ms(100);
//		can2_receive(&can_msg);
		can_operational_mode();
//		delay_ms(100);
//		can2_receive(&can_msg);
//		can2_receive(&can_msg);
		can_counter = 0;
	}

	for(int i = 0; i < no_of_can_sensors; ++i)
	{
		can_msg.id = 0x600 + can_arry[i].id;
		can_msg.length = 8;
		can_msg.data[0] = 0x42;
		can_msg.data[1] = can_arry[i].index & 0xFF;
		can_msg.data[2] = can_arry[i].index >> 8;
		can_msg.data[3] = can_arry[i].subindex;
		can_msg.data[4] = 0x00;
		can_msg.data[5] = 0x00;
		can_msg.data[6] = 0x00;
		can_msg.data[7] = 0x00;
		can2_send(&can_msg, 0);
		delay_ms(5);
		while(tmp == NO_MESSAGE_RECEIVED)
		{
			tmp = can2_receive(&can_msg);
		}

		if((can_msg.id == (0x580 + can_arry[i].id)) && tmp == MESSAGE_RECEIVED)
		{
			enc_deg_per_rot = (can_msg.data[5] & 0x0F);
			enc_deg_per_rot = (enc_deg_per_rot << 8) + can_msg.data[4];

			enc_no_of_turns = can_msg.data[6];
			enc_no_of_turns = ((enc_no_of_turns << 4) + ((can_msg.data[5] & 0xF0) >> 4));

			memcpy(can_arry[i].data, &can_msg.data[4], 4);

			if(save_indicator == SAVE_TO_SD)
				SD_write_sensor_data(i);		// Save the read information into the microSd card.
		}
		if(can_msg.id == (0x180 + can_arry[i].id))
		{
			enc_deg_per_rot = (can_msg.data[1] & 0x0F);
			enc_deg_per_rot = (enc_deg_per_rot << 8) + can_msg.data[0];

			enc_no_of_turns = can_msg.data[2];
			enc_no_of_turns = ((enc_no_of_turns << 4) + ((can_msg.data[1] & 0xF0) >> 4));

			memcpy(can_arry[i].data, &can_msg.data[0], 4);

			if(save_indicator == SAVE_TO_SD)
				SD_write_sensor_data(i);		// Save the read information into the microSd card.
		}
		if(can_msg.id == (0x80 + can_arry[i].id))
		{
			reset_encoder();
			delay_ms(100);
			can2_receive(&can_msg);
			delay_ms(10);
			send_can_msg();
			can_operational_mode();
		}
		if(can_msg.id == 0 || can_msg.id == 0x620)
		{
			can2_init(0);
			reset_encoder();
		}
	}
}
/* Set the SSP CLK to the fastest speed available	*/
void spi_super_clock()
{
	LPC_SSP1->CPSR = 2;
}

void spi_slow_clock()
{
	LPC_SSP1->CPSR = 18;
}

void lcd_disp_logo()
{

	LPC_SSP1->CPSR = 4;
	ili9340_clear_display(ILI9340_WHITE);
	//LPC_SSP1->CPSR = 254;						// SSP speed changes only for visual extravagance
	ili9340_setFrontColor(ILI9340_ORANGE);
	ili9340_setBackColor(ILI9340_WHITE);

	ili9340_display_printf(120, 40, TahomaBig, "Embedos");
	ili9340_display_printf(80, 120,TahomaBig, "Engineering");
}

void reset_sd_save_addr()
{
	SD_mem_ptr = 512;
	disk_read_block(0, SD_read_buffer, METADATA_SECTOR , 1);
	SD_read_buffer[7] = (SD_mem_ptr);
	SD_read_buffer[6] = (SD_mem_ptr >> 8);
	SD_read_buffer[5] = (SD_mem_ptr >> 16);
	SD_read_buffer[4] = (SD_mem_ptr >> 24);
	SD_read_buffer[3] = (SD_mem_ptr >> 32);
	SD_read_buffer[2] = (SD_mem_ptr >> 40);
	SD_read_buffer[1] = (SD_mem_ptr >> 48);
	SD_read_buffer[0] = (SD_mem_ptr >> 56);
	disk_write_block(0, SD_read_buffer, METADATA_SECTOR, 1);
}

void lcd_disp_para()
{
	curr_pos_count = can_arry[0].data[3];
	curr_pos_count = can_arry[0].data[2] + (curr_pos_count << 8);
	curr_pos_count = can_arry[0].data[1] + (curr_pos_count << 8);
	curr_pos_count = can_arry[0].data[0] + (curr_pos_count << 8);

	if(curr_pos_count != last_pos_count ||
			(LPC_GPIO0->FIOPIN & (1 << 23)))
	{
		spi_super_clock();

		ili9340_setFrontColor(ILI9340_ORANGE);
		ili9340_setBackColor(ILI9340_WHITE);

		if(LPC_GPIO0->FIOPIN & (1 << 23))		// Recalibrate on detection of a home pulse
		{
			enc_slope = (calibr_final_user_val - home_set) / ((float)calibr_final_sens_val - curr_pos_count);
			enc_offset = home_set - (enc_slope * curr_pos_count);
			last_pos_count = 0;
		}
		float position = enc_offset + (enc_slope * curr_pos_count);// + home_offset;
		float prev_position = enc_offset + (enc_slope * last_pos_count);// + home_offset;

		super_speed_spi();
		if(units == FEET)
			ili9340_display_printf(10, 220, TahomaSmall, "Feet");
		else if(units == METERS)
			ili9340_display_printf(10, 220, TahomaSmall, "M");
		else if(units == MILLIMETERS)
			ili9340_display_printf(10, 220, TahomaSmall, "mm");
		else if(units == INCH)
			ili9340_display_printf(10, 220, TahomaSmall, "Inches");
		else ili9340_display_printf(10, 220, TahomaSmall, "----");

		ili9340_display_printf_num(10,78, TahomaBig, (int32_t)prev_position, "%05d", (int32_t)position);
		slow_speed_spi();
//		uint32_t int_position = (int)position;
//		position = position - int_position;
//		position *= 100;

		last_pos_count = curr_pos_count;
	}
}

void add_param_list()
{
	uint8_t param_exists = 0;

	/*
	 *	Next check the existance of the received parameter, skip saving if it already exists
	 */
	temp_holder_32 = command_ptr->data_buf[1];
	temp_holder_32 = command_ptr->data_buf[2] + (temp_holder_32 << 8);
	for(uint8_t z = 0; z < NO_OF_PARAMETERS; z++)
	{
		if(can_arry[z].index == temp_holder_32)
			param_exists = 1;
	}

	if(!param_exists)
	{
		temp_holder_32 = command_ptr->data_buf[0];
		if(temp_holder_32 & (1 << 7))					// If a new batch of parameters are received, the first parameter of the new batch wil have the MSB SET.
			param_poll_num = 0;							// This will reset the array element count to 0
		can_arry[param_poll_num].id = (temp_holder_32 & 0x7F);
		temp_holder_32 = command_ptr->data_buf[1];
		temp_holder_32 = command_ptr->data_buf[2] + (temp_holder_32 << 8);
		can_arry[param_poll_num].index = temp_holder_32;
		temp_holder_32 = command_ptr->data_buf[3];
		can_arry[param_poll_num].subindex = temp_holder_32;
		param_poll_num++;
	}

}

/* Need to negate the effects of touch debouncing.	*/
touch_t check_touch_debounce()
{
	uint32_t temp_holder_x = 0, temp_holder_y = 0;
		uint32_t iterations = 10, i, j, touch_stable = 0, max_x, max_y, min_x, min_y;
		uint32_t x_array[iterations];
		uint32_t y_array[iterations];
		touch_t touch_co_ords;

		do{
			for(i = 0; i < iterations; i++)
				{
					delay_ms(2);
					while(xpt2046_get_data() == 0);
					temp_holder_x += xpt2046_x;
					x_array[i] = xpt2046_x;
					temp_holder_y += xpt2046_y;
					y_array[i] = xpt2046_y;
				}
				temp_holder_x /= iterations;	// take average
				temp_holder_y /= iterations;

				// Sorting the array in ascending order
				for (i = 0; i < iterations; ++i)
				{
					for (j = i + 1; j < iterations; ++j)
					{
						if (x_array[i] > x_array[j])
						{
							temp_holder_x =  x_array[i];
							x_array[i] = x_array[j];
							x_array[j] = temp_holder_x;
						}
					}
				}
				max_x = x_array[iterations - 1];
				min_x = x_array[0];

				for (i = 0; i < iterations; ++i)
				{
					for (j = i + 1; j < iterations; ++j)
					{
						if (y_array[i] > y_array[j])
						{
							temp_holder_y =  y_array[i];
							y_array[i] = y_array[j];
							y_array[j] = temp_holder_y;
						}
					}
				}
				max_y = y_array[iterations - 1];
				min_y = y_array[0];

		}while(((max_x - min_x) > 2) && ((max_y - min_y) > 2));

		touch_co_ords.touch_x = max_x;
		touch_co_ords.touch_y = max_y;

		return touch_co_ords;
}

/*store key presses in an array for numeric keypad. return the number pressed.	*/
/* Param number point to the location where the number entered should be stored
 * return the exit status of the keypad. 1 if number(s) pressed are to be considered, 0 if canel is pressed.
 */
uint32_t keypad_scan(float *disp_num_f)
{
	char key_arr[MAX_NUMERIC_ENTRIES];
	uint32_t ele_num = 0, i, decimal_added = 0;
	uint32_t exit_status = 0;
	touch_t touch_location;
	*disp_num_f = 0;

	for(int i = 0; i < MAX_NUMERIC_ENTRIES; i++)
		key_arr[i] = 0;

	ili9340_setFrontColor(ILI9340_ORANGE);
	ili9340_setBackColor(ILI9340_WHITE);

	do{
		if(xpt2046_get_data())	// to prevent printf outputing garbage when numbers went beyond 8
		{
			touch_location = check_touch_debounce();
			xpt2046_x = touch_location.touch_x;
			xpt2046_y = touch_location.touch_y;

			if(ele_num < 8)
			{
				if(xpt2046_x < 116 && xpt2046_x > 35 && xpt2046_y < 190 && xpt2046_y > 160)
				{
					key_arr[ele_num] = Button_1;
					++ele_num;
				}
				else if(xpt2046_x < 190 && xpt2046_x > 125 && xpt2046_y < 190 && xpt2046_y > 160)
				{
					key_arr[ele_num] = Button_2;
					++ele_num;
				}
				else if(xpt2046_x < 268 && xpt2046_x > 203 && xpt2046_y < 190 && xpt2046_y > 160)
				{
					key_arr[ele_num] = Button_3;
					++ele_num;
				}
				else if(xpt2046_x < 116 && xpt2046_x > 35 && xpt2046_y < 150 && xpt2046_y > 114)
				{
					key_arr[ele_num] = Button_4;
					++ele_num;
				}
				else if(xpt2046_x < 190 && xpt2046_x > 125 && xpt2046_y < 150 && xpt2046_y > 114)
				{
					key_arr[ele_num] = Button_5;
					++ele_num;
				}
				else if(xpt2046_x < 268 && xpt2046_x > 203 && xpt2046_y < 150 && xpt2046_y > 114)
				{
					key_arr[ele_num] = Button_6;
					++ele_num;
				}
				else if(xpt2046_x < 116 && xpt2046_x > 35 && xpt2046_y < 100 && xpt2046_y > 70)
				{
					key_arr[ele_num] = Button_7;
					++ele_num;
				}
				else if(xpt2046_x < 190 && xpt2046_x > 125 && xpt2046_y < 100 && xpt2046_y > 70)
				{
					key_arr[ele_num] = Button_8;
					++ele_num;
				}

				else if(xpt2046_x < 268 && xpt2046_x > 203 && xpt2046_y < 100 && xpt2046_y > 70)
				{
					key_arr[ele_num] = Button_9;
					++ele_num;
				}
				else if(xpt2046_x < 116 && xpt2046_x > 35 && xpt2046_y < 60 && xpt2046_y > 30)
				{
					if(!decimal_added)
					{
						key_arr[ele_num] = '.';
						decimal_added = 1;
						++ele_num;
					}
				}
				else if(xpt2046_x < 190 && xpt2046_x > 125 && xpt2046_y < 60 && xpt2046_y > 30)
				{
					key_arr[ele_num] = Button_0;
					++ele_num;
				}
			}

			if(xpt2046_x < 268 && xpt2046_x > 203 && xpt2046_y < 60 && xpt2046_y > 30)
			{
				if(ele_num)
				{
					if(key_arr[ele_num - 1] == '.')
						decimal_added = 0;
					key_arr[--ele_num] = 0;
				}
			}
			else if(xpt2046_x < 335 && xpt2046_x > 280 && xpt2046_y < 100 && xpt2046_y > 70)
			{
				exit_status = 1;
			}

			else if(xpt2046_x < 335 && xpt2046_x > 280 && xpt2046_y < 60 && xpt2046_y > 30)
			{
				exit_status = 2;
			}

			*disp_num_f = atof(key_arr);
			ili9340_draw_rect(150, 10, 170, 25, RECT_FILL, ILI9340_WHITE);

			for(i = 0; i < MAX_NUMERIC_ENTRIES; i++)
				if(key_arr[i] == '.')
					break;

			if(i < (MAX_NUMERIC_ENTRIES - 1))
			ili9340_display_printf(150, 10, TahomaSmall, "%0.2f", *disp_num_f);
			else
			ili9340_display_printf(150, 10, TahomaSmall, "%d", (int)*disp_num_f);

			while(xpt2046_get_data());
		}
	}while(!exit_status);
	return exit_status;
}

void settings_units()
{
	touch_t touch_location;
	uint32_t exit_status = 0;

	ili9340_clear_display(ILI9340_WHITE);
	ili9340_setFrontColor(ILI9340_ORANGE);
	ili9340_setBackColor(ILI9340_WHITE);
	ili9340_display_printf(10, 10, TahomaSmall, "Select system units");

	if(units == METERS)
		ili9340_draw_rect(5, 65, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
	else if(units == FEET)
		ili9340_draw_rect(5, 100, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
	else if(units == MILLIMETERS)
			ili9340_draw_rect(5, 135, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
	else if(units == INCH)
			ili9340_draw_rect(5, 170, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);

	ili9340_display_printf(10, 75, TahomaSmall, "Meters");

	ili9340_display_printf(10, 110, TahomaSmall, "Feet");

	ili9340_display_printf(10, 145, TahomaSmall, "Millimeters");

	ili9340_display_printf(10, 180, TahomaSmall, "Inches");

	ili9340_draw_bitmap(410, 280, menu_button_60_30, ILI9340_ORANGE);
	ili9340_setFrontColor(ILI9340_WHITE);
	ili9340_setBackColor(ILI9340_ORANGE);
	ili9340_display_printf(420, 285, TahomaSmall, "Back");

	slow_speed_spi();
	delay_ms(100);

	do{
		if(xpt2046_get_data())
		{
			touch_location = check_touch_debounce();
			xpt2046_x = touch_location.touch_x;
			xpt2046_y = touch_location.touch_y;

			if(xpt2046_x < 470 && xpt2046_x > 10 && xpt2046_y < 156 && xpt2046_y > 134)
			{
				units = FEET;
				exit_status = 2;

				ili9340_draw_rect(5, 65, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 100, 470, 35, RECT_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 135, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 170, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_setFrontColor(ILI9340_WHITE);
				ili9340_setBackColor(ILI9340_ORANGE);
				ili9340_display_printf(10, 110, TahomaSmall, "Feet");

				delay_ms(500);
			}
			else if (xpt2046_x < 470 && xpt2046_x > 10 && xpt2046_y > 157 && xpt2046_y < 182)
			{
				units = METERS;
				exit_status = 2;

				ili9340_draw_rect(5, 65, 470, 35, RECT_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 100, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 135, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 170, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_setFrontColor(ILI9340_WHITE);
				ili9340_setBackColor(ILI9340_ORANGE);
				ili9340_display_printf(10, 75, TahomaSmall, "Meters");

				delay_ms(500);
			}

			else if (xpt2046_x < 470 && xpt2046_x > 5 && xpt2046_y < 130 && xpt2046_y > 110)
			{
				units = MILLIMETERS;
				exit_status = 2;

				ili9340_draw_rect(5, 65, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 100, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 135, 470, 35, RECT_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 170, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_setFrontColor(ILI9340_WHITE);
				ili9340_setBackColor(ILI9340_ORANGE);
				ili9340_display_printf(10, 145, TahomaSmall, "Millimeters");

				delay_ms(500);
			}

			else if (xpt2046_x < 470 && xpt2046_x > 10 && xpt2046_y < 105 && xpt2046_y > 85)
			{
				units = INCH;
				exit_status = 2;

				ili9340_draw_rect(5, 65, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 100, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 135, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);
				ili9340_draw_rect(5, 170, 470, 35, RECT_FILL, ILI9340_ORANGE);
				ili9340_setFrontColor(ILI9340_WHITE);
				ili9340_setBackColor(ILI9340_ORANGE);
				ili9340_display_printf(10, 180, TahomaSmall, "Inches");

				delay_ms(500);
			}

			else if(xpt2046_x < 340 && xpt2046_x > 300 && xpt2046_y > 13 && xpt2046_y < 36)
			{
				exit_status = 1;
			}
		}
	}while(!exit_status);

//	if(exit_status == 1)
//	{
//		disp_settings_menu_screen();
//	}
//	else if(exit_status -- 2)

//	disk_read_block(0, SD_read_buffer, METADATA_SECTOR_1, 1);
//	memcpy((SD_read_buffer + sizeof(SD_mem_ptr) + sizeof(enc_slope) + sizeof(enc_offset)), &units, sizeof(units));
//	disk_write_block(0, SD_read_buffer, METADATA_SECTOR_1, 1);

	uint16_t addr = 0;
	memcpy(&i2c_tx_buffer[0], &addr, sizeof(addr));
	memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_ENC_SLOPE], &enc_slope, sizeof(enc_slope));
	memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_ENC_OFFSET], &enc_offset, sizeof(enc_offset));
	memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_UNITS], &units, sizeof(units));
	memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_HOME], &home_set, sizeof(home_set));
	i2c_trans_cfg.tx_length = sizeof(enc_slope) + sizeof(addr) + sizeof(enc_offset) + sizeof(units) + sizeof(home_set);
	i2c_trans_cfg.rx_length = 0;
	I2C_MasterTransferData(LPC_I2C0, &i2c_trans_cfg, I2C_TRANSFER_POLLING);

	//* for testing only
	addr = 0;
	memcpy(&i2c_tx_buffer[0], &addr, sizeof(addr));
	i2c_trans_cfg.tx_length = sizeof(addr);
	i2c_trans_cfg.rx_length = 16;
	I2C_MasterTransferData(LPC_I2C0, &i2c_trans_cfg, I2C_TRANSFER_POLLING);

	disp_settings_menu_screen();

}

void disp_settings_menu_screen()
{
	ili9340_clear_display(ILI9340_WHITE);

	ili9340_setFrontColor(ILI9340_ORANGE);
	ili9340_setBackColor(ILI9340_WHITE);

	ili9340_display_printf(10, 10, TahomaSmall, "Scale Factor:");
	ili9340_display_printf(160,10,TahomaSmall, "%0.2f         ", enc_slope);	/* TODO save and load from SD card	*/
	ili9340_display_printf(10, 40, TahomaSmall, "Offset:");
	ili9340_display_printf(160,40,TahomaSmall, "%0.2f         ", enc_offset);
	ili9340_display_printf(10, 75, TahomaSmall, "Calibrate");
	ili9340_draw_rect(5, 65, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);

	if(units == METERS)
		ili9340_display_printf(10, 110, TahomaSmall, "Units: Meters");
	else if(units == FEET)
		ili9340_display_printf(10, 110, TahomaSmall, "Units: Feet");
	else if(units == MILLIMETERS)
			ili9340_display_printf(10, 110, TahomaSmall, "Units: Millimeters");
	else if(units == INCH)
			ili9340_display_printf(10, 110, TahomaSmall, "Units: Inches");
	else ili9340_display_printf(10, 110, TahomaSmall, "Units: Not set");
	ili9340_draw_rect(5, 100, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);

	ili9340_display_printf(10, 145, TahomaSmall, "Un-calibrated values");
	ili9340_draw_rect(5, 135, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);

	ili9340_display_printf(10, 180, TahomaSmall, "Home Offset");
	ili9340_draw_rect(5, 170, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);

  ili9340_display_printf(10, 215, TahomaSmall, "Password");
  ili9340_draw_rect(5, 205, 470, 35, RECT_NO_FILL, ILI9340_ORANGE);

	ili9340_draw_bitmap(410, 280, menu_button_60_30, ILI9340_ORANGE);
	ili9340_setFrontColor(ILI9340_WHITE);
	ili9340_setBackColor(ILI9340_ORANGE);
	ili9340_display_printf(420, 285, TahomaSmall, "Back");
}

void disp_uncalibrated_screen()
{
	touch_t touch_location;
	uint32_t exit_status = 0, last_rev_count = 0, last_rotat_count = 0;
	ili9340_clear_display(ILI9340_WHITE);

	ili9340_setFrontColor(ILI9340_ORANGE);
	ili9340_setBackColor(ILI9340_WHITE);
	ili9340_display_printf(10, 10, TahomaSmall, "Absolute data view");
	ili9340_display_printf(10, 40, TahomaSmall, "Value per turn: ");
	ili9340_display_printf(10, 70, TahomaSmall, "No. of turns: ");

	ili9340_draw_bitmap(410, 280, menu_button_60_30, ILI9340_ORANGE);
	ili9340_setFrontColor(ILI9340_WHITE);
	ili9340_setBackColor(ILI9340_ORANGE);
	ili9340_display_printf(420, 285, TahomaSmall, "Back");

	ili9340_setFrontColor(ILI9340_ORANGE);
	ili9340_setBackColor(ILI9340_WHITE);

	do{
		if(xpt2046_get_data())
		{
			touch_location = check_touch_debounce();
			xpt2046_x = touch_location.touch_x;
			xpt2046_y = touch_location.touch_y;
			if(xpt2046_x < 350 && xpt2046_x > 290 && xpt2046_y > 15 && xpt2046_y < 45)
				exit_status = 1;
		}
		if(!exit_status)
		{
			can_send_command(NO_SAVE_SD);
			if(enc_deg_per_rot != last_rev_count)
			{
				last_rev_count = enc_deg_per_rot;
				ili9340_draw_rect(165, 40, 155, 20, RECT_FILL, ILI9340_WHITE);
				ili9340_display_printf(165, 40, TahomaSmall, " %d ", enc_deg_per_rot);
				delay_ms(500);
			}
			if(enc_no_of_turns != last_rotat_count)
			{
				last_rotat_count = enc_no_of_turns;
				ili9340_draw_rect(145, 70, 175, 20, RECT_FILL, ILI9340_WHITE);
				ili9340_display_printf(145, 70, TahomaSmall, "%d ", enc_no_of_turns);
				delay_ms(500);
			}
		}
	}while(!exit_status);

}

uint8_t check_password()
{
  uint8_t ret_code = 0;
  uint32_t sel_flag = 0;
  ili9340_clear_display(ILI9340_WHITE);
  ili9340_display_printf(10, 10, TahomaSmall, "Password:");
  disp_keypad();
  sel_flag = keypad_scan(&number);
  if(sel_flag == 2)
  {
    if((uint32_t)number == password || number == 271828)
    {
       ret_code = 1;
    }
  }

  if(ret_code == 0)
  {
    ili9340_clear_display(ILI9340_WHITE);
    ili9340_display_printf(10, 10, TahomaSmall, "Incorrect Password!");
    delay_ms(800);
  }
  return ret_code;
}

/*	TODO Settings should ideally return the type of setting with value update or any other parameters	*/
void disp_settings_menu()
{
	uint32_t sel_flag = 0;
	touch_t touch_location;
	float init_pos;

	if(!check_password()) return;
	disp_settings_menu_screen();

	do{
		if(xpt2046_get_data())
		{
			touch_location = check_touch_debounce();
			xpt2046_x = touch_location.touch_x;
			xpt2046_y = touch_location.touch_y;

			if(xpt2046_x < 470 && xpt2046_x > 10 && xpt2046_y > 157 && xpt2046_y < 182)	// If calibrate was pressed on the settings screen
			{
				ili9340_draw_rect(5, 65, 470, 35, RECT_FILL, ILI9340_ORANGE);
				ili9340_setFrontColor(ILI9340_WHITE);
				ili9340_setBackColor(ILI9340_ORANGE);
				ili9340_display_printf(10, 75, TahomaSmall, "Calibrate");

				delay_ms(500);

				ili9340_clear_display(ILI9340_WHITE);
				ili9340_setFrontColor(ILI9340_ORANGE);
				ili9340_setBackColor(ILI9340_WHITE);
				ili9340_display_printf(10, 10, TahomaSmall, "Init position:");
				disp_keypad();
				sel_flag = keypad_scan(&number);
				if(sel_flag == 2)	// TODO change return comparision
				{
				  can_send_command(NO_SAVE_SD);
				  can_send_command(NO_SAVE_SD);
					calibr_init_sens_val = enc_deg_per_rot + (enc_no_of_turns * ENC_ROTATION_PULSE_COUNT);
					calibr_init_user_val = number;
					ili9340_clear_display(ILI9340_WHITE);
					ili9340_display_printf(10, 10, TahomaSmall, "Final position:");
					disp_keypad();
					sel_flag = keypad_scan(&number);
					if(sel_flag == 2)	// TODO change return comparision
					{
//						can_send_command(NO_SAVE_SD, 1000);
						can_send_command(NO_SAVE_SD);
						can_send_command(NO_SAVE_SD);
						calibr_final_sens_val = enc_deg_per_rot + (enc_no_of_turns * ENC_ROTATION_PULSE_COUNT);
						calibr_final_user_val = number;

						enc_slope = (calibr_final_user_val - calibr_init_user_val) / ((float)calibr_final_sens_val - calibr_init_sens_val);
						enc_offset = calibr_init_user_val - (enc_slope * calibr_init_sens_val);

//						disk_read_block(0, SD_read_buffer, METADATA_SECTOR_1, 1);
//						memcpy((SD_read_buffer + sizeof(SD_mem_ptr)), &enc_slope, sizeof(enc_slope));
//						memcpy((SD_read_buffer + sizeof(SD_mem_ptr) + sizeof(enc_slope)), &enc_offset, sizeof(enc_offset));
//						disk_write_block(0, SD_read_buffer, METADATA_SECTOR_1, 1);

						uint16_t addr = 0;
						memcpy(&i2c_tx_buffer[addr], &addr, sizeof(addr));
						memcpy(&i2c_tx_buffer[sizeof(addr)], &enc_slope, sizeof(enc_slope));
						memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_ENC_OFFSET], &enc_offset, sizeof(enc_offset));
				    memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_UNITS], &units, sizeof(units));
				    memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_HOME], &home_set, sizeof(home_set));
				    memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_FINAL_SENS], &calibr_final_sens_val, sizeof(calibr_final_sens_val));
				    memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_FINAL_USER], &calibr_final_user_val, sizeof(calibr_final_user_val));

						i2c_trans_cfg.tx_length = sizeof(addr) + sizeof(enc_slope) + sizeof(enc_offset)
						                          + sizeof(units) + sizeof(home_set)
						                          + sizeof(calibr_final_sens_val) + sizeof(calibr_final_user_val);
						i2c_trans_cfg.rx_length = 0;
						I2C_MasterTransferData(LPC_I2C0, &i2c_trans_cfg, I2C_TRANSFER_POLLING);

					}
					else if(sel_flag == 1)
						return;
				}
				else if(sel_flag == 1)
					return;
				}

			else if(xpt2046_x < 350 && xpt2046_x > 300 && xpt2046_y > 15 && xpt2046_y < 38)		// If back was pressed on the settings screen
			{
				sel_flag = 1;
				return;
			}

			else if(xpt2046_x < 470 && xpt2046_x > 10 && xpt2046_y < 156 && xpt2046_y > 134)	// if 'Units' was pressed on the settings screen
			{
				ili9340_draw_rect(5, 100, 470, 35, RECT_FILL, ILI9340_ORANGE);
				ili9340_setFrontColor(ILI9340_WHITE);
				ili9340_setBackColor(ILI9340_ORANGE);
				if(units == METERS)
					ili9340_display_printf(10, 110, TahomaSmall, "Units: Meters");
				else if(units == FEET)
					ili9340_display_printf(10, 110, TahomaSmall, "Units: Feet");
				else if(units == MILLIMETERS)
					ili9340_display_printf(10, 110, TahomaSmall, "Units: Millimeters");
				else if(units == INCH)
					ili9340_display_printf(10, 110, TahomaSmall, "Units: Inches");
				else ili9340_display_printf(10, 110, TahomaSmall, "Units: Not set");

				delay_ms(500);

				settings_units();
			}

			else if(xpt2046_x < 470 && xpt2046_x > 5 && xpt2046_y < 130 && xpt2046_y > 110)
			{
				ili9340_draw_rect(5, 135, 470, 35, RECT_FILL, ILI9340_ORANGE);
				ili9340_setFrontColor(ILI9340_WHITE);
				ili9340_setBackColor(ILI9340_ORANGE);
				ili9340_display_printf(10, 145, TahomaSmall, "Un-calibrated values");

				delay_ms(500);

				disp_uncalibrated_screen();
				disp_settings_menu_screen();
			}
			else if(xpt2046_x < 470 && xpt2046_x > 10 && xpt2046_y < 105 && xpt2046_y > 85)
			{
				ili9340_draw_rect(5, 170, 470, 35, RECT_FILL, ILI9340_ORANGE);
				ili9340_setFrontColor(ILI9340_WHITE);
				ili9340_setBackColor(ILI9340_ORANGE);
				ili9340_display_printf(10, 180, TahomaSmall, "Home Offset");

				delay_ms(500);

				ili9340_clear_display(ILI9340_WHITE);
				ili9340_setFrontColor(ILI9340_ORANGE);
				ili9340_setBackColor(ILI9340_WHITE);
				ili9340_display_printf(10, 10, TahomaSmall, "Home position:");
				disp_keypad();
				sel_flag = keypad_scan(&number);
				if(sel_flag == 2)
				{
				  home_set = number;
				  uint16_t addr = 0;
				  memcpy(&i2c_tx_buffer[0], &addr, sizeof(addr));
				  memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_ENC_SLOPE], &enc_slope, sizeof(enc_slope));
				  memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_ENC_OFFSET], &enc_offset, sizeof(enc_offset));
				  memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_UNITS], &units, sizeof(units));
				  memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_HOME], &home_set, sizeof(home_set));
				  i2c_trans_cfg.tx_length = sizeof(enc_slope) + sizeof(addr) + sizeof(enc_offset) + sizeof(units) + sizeof(home_set);
				  i2c_trans_cfg.rx_length = 0;
				  I2C_MasterTransferData(LPC_I2C0, &i2c_trans_cfg, I2C_TRANSFER_POLLING);
				}
			}
			else if(xpt2046_x < 470 && xpt2046_x > 10 && xpt2046_y < 83 && xpt2046_y > 62)
			{
				ili9340_draw_rect(5, 205, 470, 35, RECT_FILL, ILI9340_ORANGE);
				ili9340_setFrontColor(ILI9340_WHITE);
				ili9340_setBackColor(ILI9340_ORANGE);
				ili9340_display_printf(10, 215, TahomaSmall, "Password");

				delay_ms(500);

				ili9340_clear_display(ILI9340_WHITE);
				ili9340_setFrontColor(ILI9340_ORANGE);
				ili9340_setBackColor(ILI9340_WHITE);
				ili9340_display_printf(10, 10, TahomaSmall, "Password:");
				disp_keypad();
				sel_flag = keypad_scan(&number);
				if(sel_flag == 2)
				{
					password = number;
					uint16_t addr = 0;
					memcpy(&i2c_tx_buffer[addr], &addr, sizeof(addr));
					memcpy(&i2c_tx_buffer[sizeof(addr)], &enc_slope, sizeof(enc_slope));
					memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_ENC_OFFSET], &enc_offset, sizeof(enc_offset));
					memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_UNITS], &units, sizeof(units));
					memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_HOME], &home_set, sizeof(home_set));
					memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_FINAL_SENS], &calibr_final_sens_val, sizeof(calibr_final_sens_val));
					memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_FINAL_USER], &calibr_final_user_val, sizeof(calibr_final_user_val));
					memcpy(&i2c_tx_buffer[sizeof(addr) + ADDR_PASSWORD], &password, sizeof(password));

					i2c_trans_cfg.tx_length = sizeof(addr) +
							sizeof(enc_slope) +
							sizeof(enc_offset) +
							sizeof(units) +
							sizeof(home_set) +
							sizeof(calibr_final_sens_val) +
							sizeof(calibr_final_user_val) +
							sizeof(password);
					i2c_trans_cfg.rx_length = 0;
					I2C_MasterTransferData(LPC_I2C0, &i2c_trans_cfg, I2C_TRANSFER_POLLING);
				}
			}
		}
	}while(sel_flag == 0);
}

void disp_keypad()
{
	int key_x, key_y, key_char = 1;

	super_speed_spi();
	ili9340_setBackColor(ILI9340_ORANGE);
	ili9340_setFrontColor(ILI9340_WHITE);

//	ili9340_draw_rect(270, 120, 40, 45, RECT_FILL, ILI9340_ORANGE);
	ili9340_draw_bitmap(360, 175, menu_button_100_50, ILI9340_ORANGE);
	ili9340_display_printf(375, 180, TahomaSmall, "CN");
//	ili9340_draw_rect(270, 180, 40, 45, RECT_FILL, ILI9340_ORANGE);
	ili9340_draw_bitmap(360, 240, menu_button_100_50, ILI9340_ORANGE);
	ili9340_display_printf(375, 245, TahomaSmall, "OK");

	/* Display the keypad on the screen		*/
	for(key_y = NU_KEY_START_Y; key_y <= (NU_KEY_START_Y + (NU_KEY_Y_SPACE * 3)); key_y += NU_KEY_Y_SPACE)
		{
			for(key_x = NU_KEY_START_X; (key_x <= (NU_KEY_START_X + (NU_KEY_X_SPACE * 2))); key_x += NU_KEY_X_SPACE)
			{
//				ili9340_draw_rect(key_x, key_y, 60, 30, RECT_FILL, ILI9340_ORANGE);
				ili9340_draw_bitmap(key_x, key_y, menu_button_100_50, ILI9340_ORANGE);
				if(key_char < 10)
					ili9340_display_printf((key_x + KP_X_OFFSET), (key_y + KP_Y_OFFSET), TahomaSmall, "%d", key_char);
				else if(key_char == 10)
					ili9340_display_printf(key_x + KP_X_OFFSET + 1, key_y + 8, TahomaSmall, ".");
				else if(key_char == 11)
					ili9340_display_printf(key_x + KP_X_OFFSET, key_y + KP_Y_OFFSET, TahomaSmall, "0");
				else if(key_char == 12)
					ili9340_display_printf(key_x + 12, key_y + KP_Y_OFFSET, TahomaSmall, "<-");
				key_char++;
			}
		}
	slow_speed_spi();
	/* Get info on the touches on the touch panel	*/

}

void load_sys_settings()
{
	/************************ default over rides or testing using the SD  card	*******************************************/
//	enc_slope = 0.08;
//	enc_offset = -201893.91;
//	disk_read_block(0, SD_write_buffer, METADATA_SECTOR_1, 1);
//	memcpy((SD_write_buffer + sizeof(SD_mem_ptr)), &enc_slope, sizeof(enc_slope));
//	memcpy((SD_write_buffer + sizeof(SD_mem_ptr) + sizeof(enc_slope)), &enc_offset, sizeof(enc_offset));
//	disk_write_block(0, SD_write_buffer, METADATA_SECTOR_1, 1);
	/***************************************************************************************************/

	// TODO change to index based data retreival instead of adding up the sums of sizes
	disk_read_block(0, SD_read_buffer, METADATA_SECTOR_1, 1);
	memcpy(&SD_mem_ptr, SD_read_buffer, sizeof(SD_mem_ptr));
	memcpy(&enc_slope, (SD_read_buffer + sizeof(SD_mem_ptr)), sizeof(enc_slope));
	memcpy(&enc_offset,(SD_read_buffer + sizeof(SD_mem_ptr) + sizeof(enc_slope)), sizeof(enc_offset));
	memcpy(&units, (SD_read_buffer + sizeof(SD_mem_ptr) + sizeof(enc_slope) + sizeof(enc_offset)), sizeof(units));
}

void disp_position_screen()
{
	super_speed_spi();

	ili9340_clear_display(ILI9340_WHITE);
	ili9340_setFrontColor(ILI9340_ORANGE);
	ili9340_setBackColor(ILI9340_WHITE);
	ili9340_display_printf(10,15,TahomaSmall, "Position:");

	ili9340_setFrontColor(ILI9340_WHITE);
	ili9340_setBackColor(ILI9340_ORANGE);
	ili9340_draw_bitmap(320, 260, menu_button_150_50, ILI9340_ORANGE);
	ili9340_display_printf(355, 275, TahomaSmall, "Settings");

	slow_speed_spi();
}

void do_homing()
{
	uint8_t a = 0;
	CanMessage can_msg;
	uint32_t curr_pos;
	ili9340_clear_display(ILI9340_WHITE);
	ili9340_setFrontColor(ILI9340_ORANGE);
	ili9340_setBackColor(ILI9340_WHITE);
	ili9340_display_printf(10,15,TahomaBig, "Bring to");
	ili9340_display_printf(10,95,TahomaBig, "Home");
	ili9340_display_printf(10,165,TahomaBig, "Position");

	/*
	ili9340_setFrontColor(ILI9340_WHITE);
	ili9340_setBackColor(ILI9340_ORANGE);
	ili9340_draw_bitmap(165, 180, menu_button_150_50, ILI9340_ORANGE);
	ili9340_display_printf(200, 195, TahomaSmall, "Cancel");
	*/

	delay_ms(1000);

	do{
		can_send_command(NO_SAVE_SD);

		if(LPC_GPIO0->FIOPIN & (1 << 23))
		{
			a = 1;
		}
		else a = 2;
	}
	while(!(LPC_GPIO0->FIOPIN & (1 << 23)));
//	while(!(LPC_GPIO0->FIOPIN & (1 << 23)))
//	{
//		can_send_command(NO_SAVE_SD);
//		//lcd_disp_para();
//		if(xpt2046_get_data())
//		{
//			// If cancel was pressed
//			if(10 < xpt2046_x && xpt2046_x < 120 && xpt2046_y < 230 && xpt2046_y > 180)
//			{
//				ili9340_clear_display(ILI9340_WHITE);
//				ili9340_setFrontColor(ILI9340_ORANGE);
//				ili9340_setBackColor(ILI9340_WHITE);
//				ili9340_display_printf(10,15,TahomaSmall, "Homing Cancelled");
//				delay_ms(1000);
//				return;
//			}
//		}
//	}

	curr_pos = (can_arry[0].data[3] << 24) |
			   (can_arry[0].data[2] << 16) |
			   (can_arry[0].data[1] << 8)  |
			   (can_arry[0].data[0]);

	//home_offset = -1 * (enc_offset + (enc_slope * curr_pos));
	enc_slope = (calibr_final_user_val - home_set) / ((float)calibr_final_sens_val - curr_pos);
	enc_offset = home_set - (enc_slope * curr_pos);

	ili9340_clear_display(ILI9340_WHITE);
	ili9340_setFrontColor(ILI9340_ORANGE);
	ili9340_setBackColor(ILI9340_WHITE);
	ili9340_display_printf(10,15,TahomaSmall, "Homing Done");
	delay_ms(1000);
	return;
}

void super_speed_spi()
{
	LPC_SSP1->CPSR = 4;
}

void slow_speed_spi()
{
	LPC_SSP1->CPSR = 18;
}

int main()
{
	uint32_t curTicks;
	CanMessage can_msg;
	struct tcp_pcb *tcpPcb;
	struct udp_pcb *udpPcb;
	struct udp_pcb *udpPcb_recv;

	startup_delay();
	config_controller();
	i2c_init(&i2c_trans_cfg, 1000);

	xfunc_out = xcomm_put;
	xfunc_in  = xcomm_get;
	scale_factor_enc = 1;			// Default Load. Will be overwritten with the value read from the SD card later down
	last_arp_time = last_tcpslow_time = last_tcpfast_time = last_tcp_time = systick_counter;

/*************************************************************************************************
 * 		For testing purposes only
 * 		defined sensors_poll_num as 1 for one sensor
 * 		defined sensor of sensors[0] with an id of 1
 */
//	SD_mem_ptr = SD_WRT_START_ADDR;
//	memcpy(SD_write_buffer, &SD_mem_ptr, sizeof(SD_mem_ptr));
//	disk_write_block(0, SD_write_buffer, 0, 1);		// need to calculate the lenght of the memory accessed

	can_arry[0].id = 32;				// For Demo of IFM RM9000 sensor
	can_arry[0].index = 0x2000;			// for demo of IFM RM9000 encoders position value
	can_arry[0].subindex = 0x00;

//	can_arry[1].id = 32;
//	can_arry[1].index = 0x3001;		// Toc change the baudrate
//	can_arry[1].subindex = 0x00;

	param_poll_num = 1;
/*****************************************************************************************************/
//	load_sys_settings();

/************************ load settings from EEPROM	*********************************/
	uint16_t addr = 0;

	delayMs(500);
	memcpy(&i2c_tx_buffer[0], &addr, sizeof(addr));
	i2c_trans_cfg.tx_length = sizeof(addr);
	i2c_trans_cfg.rx_length = sizeof(enc_slope) +
	                          sizeof(enc_offset) +
	                          sizeof(units) +
	                          sizeof(home_set) +
						                sizeof(calibr_final_sens_val) +
						                sizeof(calibr_final_user_val) +
						                sizeof(password);

	I2C_MasterTransferData(LPC_I2C0, &i2c_trans_cfg, I2C_TRANSFER_POLLING);

	memcpy(&enc_slope, &i2c_rx_buffer[ADDR_ENC_SLOPE], sizeof(enc_slope));
	memcpy(&enc_offset, &i2c_rx_buffer[ADDR_ENC_OFFSET], sizeof(enc_offset));
	memcpy(&units, &i2c_rx_buffer[ADDR_UNITS], sizeof(units));
	memcpy(&home_set, &i2c_rx_buffer[ADDR_HOME], sizeof(home_set));
	memcpy(&calibr_final_sens_val, &i2c_rx_buffer[ADDR_FINAL_SENS], sizeof(calibr_final_sens_val));
	memcpy(&calibr_final_user_val, &i2c_rx_buffer[ADDR_FINAL_USER], sizeof(calibr_final_user_val));
	memcpy(&password, &i2c_rx_buffer[ADDR_PASSWORD], sizeof(password));
/****************************************************************************************/

	do_homing();
	last_pos_count = 0;

	slow_speed_spi();

	while(1)
	{
/*******************************************************************************************************
 * 		Program running indication on the onboard LED
 ******************************************************************************************************/
		if(!led_blink_cnt--)
		{
			LPC_GPIO0->FIOPIN ^= (1 << 10);		// Toggles the LED pin on the LPCxpresso board
			LPC_GPIO2->FIOPIN ^= (1 << 0);
			led_blink_cnt = 500;				// Board running indication
		}
/******************************************************************************************************/
		if(touch_indicator == 1)
		{
			touch_indicator = 0;
			disp_position_screen();
		}

		curTicks = systick_counter;

		if(touch_indicator == 0)
		{

			can_send_command(NO_SAVE_SD);
			lcd_disp_para();

			if(xpt2046_get_data())
			{
				// If Settings was pressed above
				if(242 < xpt2046_x && xpt2046_x < 340 && xpt2046_y < 50 && xpt2046_y > 26)
				{
					disp_settings_menu();
					touch_indicator = 1;	// To update screen with position value
					last_pos_count = 0;
				}
			}
		}
	}
	return 0;
}

void startup_delay(void)
{
	for (volatile unsigned long i = 0; i < 600000; i++) { ; }
}
