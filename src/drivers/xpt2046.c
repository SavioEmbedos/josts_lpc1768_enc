/*
 * xpt2046.c
 *
 *  Created on: 22-Apr-2017
 *      Author: acdm
 */
#include <LPC17xx.h>
#include "xpt2046.h"

#include "lpc17xx_spi.h"
#include "delay.h"

#define Z_THRESHOLD 0x15

#define TUCH_PORT			LPC_GPIO2->FIOPIN
#define TUCH_DIR			LPC_GPIO2->FIODIR
#define TUCH_CS_PIN			(1 << 3)
#define TS_CS_SET 			LPC_GPIO2->FIOPIN |= (1 << 3)
#define TS_CS_CLR 			LPC_GPIO2->FIOPIN &= ~(1 << 3)

uint16_t spi_rw_16(uint8_t data)
{
    uint16_t tmp;
    SPI_SendByte(0xC1);
    tmp = SPI_SendByte(0x00);
    tmp <<= 8;
    tmp |= SPI_SendByte(0x00);
    return tmp;
}

void xpt2046_init()
{
	SPI_Init();
	TUCH_DIR |= TUCH_CS_PIN;	// Set the direction as output
    TS_CS_SET;					// Keep it de-selected
}

uint8_t xpt2046_get_data()
{
	uint8_t ret_val = 0;
	TS_CS_CLR;
	SPI_SendByte(0xB0);
	int16_t z1 = SPI_SendByte(0x00) & 0x7F;
	int z = z1;

	SPI_SendByte(0xC0);
	int16_t z2 = (255 - SPI_SendByte(0x00)) & 0x7F;
	z += z2;
	TS_CS_SET;

	if (z >= Z_THRESHOLD)
	{
		TS_CS_CLR;
		SPI_SendByte(0x90);
		data[0] = SPI_SendByte(0x00);
		data[1] = SPI_SendByte(0x00);

		SPI_SendByte(0x90);
		data[2] = SPI_SendByte(0x00);
		data[3] = SPI_SendByte(0x00);

		SPI_SendByte(0x90);
		data[4] = SPI_SendByte(0x00);
		data[5] = SPI_SendByte(0x00);

		xpt2046_x = (data[2] << 8) | data[3];
		xpt2046_x >>= 3;
		xpt2046_x -= 300;
		if(xpt2046_x < 0) xpt2046_x = 0;
		xpt2046_x /= 10;

		SPI_SendByte(0xD0);
		data[0] = SPI_SendByte(0x00);
		data[1] = SPI_SendByte(0x00);

		SPI_SendByte(0xD0);
		data[2] = SPI_SendByte(0x00);
		data[3] = SPI_SendByte(0x00);

		SPI_SendByte(0xD0);
		data[4] = SPI_SendByte(0x00);
		data[5] = SPI_SendByte(0x00);

		TS_CS_SET;

		xpt2046_y = (data[2] << 8) | data[3];
		xpt2046_y >>= 3;
		xpt2046_y -= 280;
		if(xpt2046_y < 0) xpt2046_y = 0;
		xpt2046_y = (int16_t)((float)xpt2046_y * (float)0.07);

		ret_val = 1;
	}
	return ret_val;
}
