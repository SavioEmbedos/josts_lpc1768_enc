#include "lpc17xx.h"
#include "24LC16.h"
#include "timer.h"
#include "i2c.h"

void EEPROMInit(uint32_t peripheral)
{
	TimerInit(2,500);

	switch (peripheral) {
		case 0:
			I2C0Init();
			break;
		case 1:
			I2C1Init(selected_port);
			break;
		case 2:
			I2C2Init();
			break;

	i2c_address_read = (Block0  | (1<<0));
	i2c_address_write = (Block0 | (0<<0));

	}
}

void EEPROMSetBlock(uint8_t blocknr)
{
	switch (blocknr) {
		case 0:
			i2c_address_read = (Block0  | (1<<0));
			i2c_address_write = (Block0 | (0<<0));
			break;
		case 1:
			i2c_address_read = (Block1  | (1<<0));
			i2c_address_write = (Block1 | (0<<0));
			break;
		case 2:
			i2c_address_read = (Block2  | (1<<0));
			i2c_address_write = (Block2 | (0<<0));
			break;
		case 3:
			i2c_address_read = (Block3  | (1<<0));
			i2c_address_write = (Block3 | (0<<0));
			break;
		case 4:
			i2c_address_read = (Block4  | (1<<0));
			i2c_address_write = (Block4 | (0<<0));
			break;
		case 5:
			i2c_address_read = (Block5  | (1<<0));
			i2c_address_write = (Block5 | (0<<0));
			break;
		case 6:
			i2c_address_read = (Block6  | (1<<0));
			i2c_address_write = (Block6 | (0<<0));
			break;
		case 7:
			i2c_address_read = (Block7  | (1<<0));
			i2c_address_write = (Block7 | (0<<0));
			break;
	}
}

void EEPROMWrite(uint32_t peripheral, uint32_t memory_address, uint8_t value)
{
	uint32_t portused = peripheral;

	I2CWriteLength[portused] = 3;
	I2CReadLength[portused] = 0;
	I2CMasterBuffer[portused][0] = i2c_address_write;
	I2CMasterBuffer[portused][1] = memory_address;
	I2CMasterBuffer[portused][2] = value;
	I2CEngine(portused);
	delayMs(2,5);
}

uint8_t EEPROMRead(uint32_t peripheral,uint32_t memory_address)
{
	uint8_t value;
	uint32_t portused = peripheral;

	I2CWriteLength[portused] = 2;
	I2CReadLength[portused] = 1;
	I2CMasterBuffer[portused][0] = i2c_address_write;
	I2CMasterBuffer[portused][1] = memory_address;
	I2CEngine(portused);

	I2CWriteLength[portused] = 1;
	I2CMasterBuffer[portused][0] = i2c_address_read;
	I2CEngine(portused);
	delayMs(2,5);

	value = I2CSlaveBuffer[portused][0];

	return value;
}
