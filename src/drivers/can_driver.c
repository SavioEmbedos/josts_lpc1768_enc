#include <LPC17xx.h>
#include "can_driver.h"

static void can2_reset_error( void );
static void can1_reset_error( void );

/**
 * Setup the CAN peripheral.
 *
 * The pins for the 2nd CAN peripheral are set to the right
 * mode, the power and clock are setup for the CAN peripheral
 * and CAN peripheral 2 is cleared and setup (timing and such).
 */
void can2_init(uint8_t baud_rate)
{
	// Set the processor pins to the correct mode
	LPC_PINCON->PINSEL0 &= ~(0x2<<8);  // Reset the function of Pin 0.4
	LPC_PINCON->PINSEL0 |= (0x2<<8);   // Set the function of Pin 0.4 to RD2
	LPC_PINCON->PINSEL0 &= ~(0x2<<10); // Reset the function of Pin 0.5
	LPC_PINCON->PINSEL0 |= (0x2<<10);  // Set the function of Pin 0.5 to TD2

	// Setup the power and clock for the peripheral
	LPC_SC->PCONP    |= (1<<14);  // Set the power bit for CAN peripheral 2
	LPC_SC->PCLKSEL0 &= ~(3<<28); // Clear the 2 CAN clock divider bits for can peripheral 2
	LPC_SC->PCLKSEL0 |= (1<<28);  // Set the clock divider for CAN peripheral 2 to 1,
	                              // so the main clock is the CAN clock
	LPC_SC->PCLKSEL0 &= ~((uint32_t)3<<30); // Clear the 2 clock divider bits for the can acceptance filter
	LPC_SC->PCLKSEL0 |= (1<<30);  // Set the clock divider for acceptance filter to 1

	// Setup peripheral related settings
	LPC_CAN2->MOD = 0x1; // Set the CAN peripheral in reset mode so you can change values
	LPC_CAN2->IER = 0x0; // Turn off all CAN related interrupts, they would not fire anyway
	LPC_CAN2->GSR = 0x0; // Set the error counters to 0
	// Clear everything you can via the command register
	LPC_CAN2->CMR = (0x1<<1) | // Abort transmission bit
	                (0x1<<2) | // Release receive buffer
	                (0x1<<3);  // Clear data overrun bit

	switch(baud_rate)
	{
	  case 0:
	  LPC_CAN2->BTR = 0x91C3E7; // Set the bit rate of the CAN peripheral to 20kbit/s
	                            // BRP   = 999
	                            // SJW   = 3
	                            // TESG1 = 1
	                            // TESG2 = 1
	                            // SAM   = 1
	    break;
	  case 1:
	  LPC_CAN2->BTR = 0xB4C0C7; // Set the bit rate of the CAN peripheral to 50kbit/s
	                            // BRP   = 199
	                            // SJW   = 3
	                            // TESG1 = 4
	                            // TESG2 = 3
	                            // SAM   = 1
	    break;
	  case 2:
	  LPC_CAN2->BTR = 0xB4C063; // Set the bit rate of the CAN peripheral to 100kbit/s
	                            // BRP   = 99
	                            // SJW   = 3
	                            // TESG1 = 4
	                            // TESG2 = 3
	                            // SAM   = 1
	    break;
	  case 3:
	  LPC_CAN2->BTR = 0xA3C063; // Set the bit rate of the CAN peripheral to 125kbit/s
	                            // BRP   = 99
	                            // SJW   = 3
	                            // TESG1 = 4
	                            // TESG2 = 1
	                            // SAM   = 1
	    break;
	  case 4:
		  LPC_CAN2->BTR = 0xA3C031; // Set the bit rate of the CAN peripheral to 250kbit/s
	                            // BRP   = 49
	                            // SJW   = 3
	                            // TESG1 = 3
	                            // TESG2 = 2
	                            // SAM   = 1
    break;
	  case 5:
	    LPC_CAN2->BTR = 0xA3C018; // Set the bit rate of the CAN peripheral to 500kbit/s
	                            // BRP   = 24
	                            // SJW   = 3
	                            // TESG1 = 3
	                            // TESG2 = 2
	                            // SAM   = 1
	    	  break;

	  case 6:
	  	    LPC_CAN2->BTR = 0x91C018; // Set the bit rate of the CAN peripheral to 800kbit/s
	  	                            // BRP   = 24
	  	                            // SJW   = 3
	  	                            // TESG1 = 1
	  	                            // TESG2 = 1
	  	                            // SAM   = 1
	  	    	  break;

	  case 7:
	  	  	    LPC_CAN2->BTR = 0xB4C009; // Set the bit rate of the CAN peripheral to 1000kbit/s
	  	  	                            // BRP   = 9
	  	  	                            // SJW   = 3
	  	  	                            // TESG1 = 4
	  	  	                            // TESG2 = 3
	  	  	                            // SAM   = 1
	  	  	    	  break;
	}

	LPC_CAN2->MOD = 0; // Enable the CAN peripheral again

	LPC_CANAF->AFMR |= (1<<1); // Set the acceptance filter in bypass mode
}

/**
 * Setup the CAN peripheral.
 *
 * The pins for the 2nd CAN peripheral are set to the right
 * mode, the power and clock are setup for the CAN peripheral
 * and CAN peripheral 2 is cleared and setup (timing and such).
 */
void can1_init( void ) 
{
	// Set the processor pins to the correct mode
	LPC_PINCON->PINSEL0 &= ~(0x2<<0);  // Reset the function of Pin 0.0
	LPC_PINCON->PINSEL0 |= (0x2<<0);   // Set the function of Pin 0.0 to RD1
	LPC_PINCON->PINSEL0 &= ~(0x2<<2); // Reset the function of Pin 0.1
	LPC_PINCON->PINSEL0 |= (0x2<<2);  // Set the function of Pin 0.1 to TD1

	// Setup the power and clock for the peripheral
	LPC_SC->PCONP    |= (1<<13);  // Set the power bit for CAN peripheral 1
	LPC_SC->PCLKSEL0 &= ~(3<<26); // Clear the 2 CAN clock divider bits for can peripheral 1
	LPC_SC->PCLKSEL0 |= (1<<26);  // Set the clock divider for CAN peripheral 2 to 1,
	                              // so the main clock is the CAN clock
	LPC_SC->PCLKSEL0 &= ~((uint32_t)3<<30); // Clear the 2 clock divider bits for the can acceptance filter
	LPC_SC->PCLKSEL0 |= (1<<30);  // Set the clock divider for acceptance filter to 1

	// Setup peripheral related settings
	LPC_CAN1->MOD = 0x1; // Set the CAN peripheral in reset mode so you can change values
	LPC_CAN1->IER = 0x0; // Turn off all CAN related interrupts, they would not fire anyway
	LPC_CAN1->GSR = 0x0; // Set the error counters to 0
	// Clear everything you can via the command register
	LPC_CAN1->CMR = (0x1<<1) | // Abort transmission bit
	                (0x1<<2) | // Release receive buffer
	                (0x1<<3);  // Clear data overrun bit

	LPC_CAN1->BTR = 0xDCC001; // Set the bit rate of the CAN peripheral to 100kbit/s
	                          // BRP   = 1
	                          // SJW   = 3
	                          // TESG1 = 12
	                          // TESG2 = 5
	                          // SAM   = 1

	//LPC_CAN1->MOD = 0; // Enable the CAN peripheral again  
  LPC_CAN1->MOD = (1 << 2); // Self test mode
  

	LPC_CANAF->AFMR |= (1<<1); // Set the acceptance filter in bypass mode
}

/**
 * Deinitialize the CAN peripheral 2.
 */
void can2_deinit( void ) 
{
	// Disable power to the CAN block
	LPC_SC->PCONP &= ~(1<<14);

	// Reset the pins
	LPC_PINCON->PINSEL0 &= ~(0x2<<8);  // Reset the function of Pin 0.4
	LPC_PINCON->PINSEL0 &= ~(0x2<<10); // Reset the function of Pin 0.5
}

/**
 * Deinitialize the CAN peripheral 1.
 */
void can1_deinit( void ) 
{
	// Disable power to the CAN block
	LPC_SC->PCONP &= ~(1<<13);

	// Reset the pins
	LPC_PINCON->PINSEL0 &= ~(0x2<<0);  // Reset the function of Pin 0.0
	LPC_PINCON->PINSEL0 &= ~(0x2<<2); // Reset the function of Pin 0.1
}

/**
 * Resets the transmit and receive error counts.
 */
static void can2_reset_error( void ) 
{

	LPC_CAN2->MOD |= 1; // Go into reset mode so error counters can be changed
	LPC_CAN2->GSR &= (uint32_t)0xffff << 16; // Reset transmit and receive error counters
	LPC_CAN2->MOD &= 0; // Change to normal mode again
}

/**
 * Resets the transmit and receive error counts.
 */
static void can1_reset_error( void ) 
{

	LPC_CAN1->MOD |= 1; // Go into reset mode so error counters can be changed
	LPC_CAN1->GSR &= (uint32_t)0xffff << 16; // Reset transmit and receive error counters
	LPC_CAN1->MOD &= 0; // Change to normal mode again
}

/**
 * Receive a message over the CAN peripheral.
 *
 * @param[out] msg The message object to load the
 *                 received data into.
 * @return If a message was received MESSAGE_RECEIVED,
 *         otherwise NO_MESSAGE_RECEIVED.
 */
CanReceiveStatus can2_receive( CanMessage *msg ) 
{
  uint32_t tmp = LPC_CAN2->RDA;
	if( (LPC_CAN2->SR & 1) == 0 ) // If we have not received a message return

	// Copy the message out of the register to the *msg object
	msg->length  = (LPC_CAN2->RFS>>16) & 0xF; // Get the length of the message
	msg->id      = LPC_CAN2->RID & 4095;     // Get the ID of the message

	msg->data[0] = (tmp>>0 ) & 0xFF;         // Get the data sent in the message
	msg->data[1] = (tmp>>8 ) & 0xFF;
	msg->data[2] = (tmp>>16) & 0xFF;
	msg->data[3] = (tmp>>24) & 0xFF;
	tmp = LPC_CAN2->RDB;
	msg->data[4] = (tmp>>0 ) & 0xFF;
	msg->data[5] = (tmp>>8 ) & 0xFF;
	msg->data[6] = (tmp>>16) & 0xFF;
	msg->data[7] = (tmp>>24) & 0xFF;

	if(tmp == 0)
	{
	    return ZERO_DATA_RECVD;
	}

	LPC_CAN2->CMR = (1<<2); // Release the receive buffer

	can2_reset_error();

	return MESSAGE_RECEIVED;
}

/**
 * Send a message over the CAN peripheral and busy
 * wait until we are sure the message was sent.
 *
 * Only buffer 1 is used in this implementation.
 *
 * @param[in] msg The message to send over the CAN bus.
 */
uint8_t can2_send( CanMessage *msg, uint8_t is_rtr)
{
	uint8_t cnt = 0;
	// Copy the message date from the message to the registers
	LPC_CAN2->TFI1 &= ~(0xF<<16);          // Clear the length of the message to send
	LPC_CAN2->TFI1 |= (msg->length<<16);   // Set the length of the message to send
	if(is_rtr)
		LPC_CAN2->TFI1 |= (30 << 1);
	else
		LPC_CAN2->TFI1 &= ~(30 << 1);

	LPC_CAN2->TID1  = (msg->id<<0);        // Set the ID of the message to be transmitted
	LPC_CAN2->TDA1  = (msg->data[0]<<0 ) | // Set the data of the message to be transmitted
	                  (msg->data[1]<<8 ) |
	                  (msg->data[2]<<16) |
	                  (msg->data[3]<<24);
	LPC_CAN2->TDB1  = (msg->data[4]<<0 ) |
	                  (msg->data[5]<<8 ) |
	                  (msg->data[6]<<16) |
	                  (msg->data[7]<<24);

	// Request for the processor to send the message
	LPC_CAN2->CMR = (1<<0) | // This is a transmission request
	                (1<<5);  // Use transmit buffer 1

	// Wait for the message to be transmitted
	while( (LPC_CAN2->SR & (1<<2)) == 0 && (LPC_CAN2->SR & (1<<3)) == 0)
	{
//		cnt++;
//		if(cnt == 100)
//		{
//			LPC_CAN2->CMR |= (1 << 1); // Abort tx
//			can2_reset_error();
//			return 1;
//		}
//		delay_ms(1);
	}

	can2_reset_error();
	return 0;
}

/**
 * Receive a message over the CAN peripheral.
 *
 * @param[out] msg The message object to load the
 *                 received data into.
 * @return If a message was received MESSAGE_RECEIVED,
 *         otherwise NO_MESSAGE_RECEIVED.
 */
CanReceiveStatus can1_receive( CanMessage *msg ) 
{
  uint32_t tmp;
	if( (LPC_CAN1->SR & 1) == 0 ) // If we have not received a message return
		return NO_MESSAGE_RECEIVED;

	// Copy the message out of the register to the *msg object
	msg->length  = (LPC_CAN1->RFS>>16) & 0xF; // Get the length of the message
	msg->id      = LPC_CAN1->RID & 4095;     // Get the ID of the message
  tmp = LPC_CAN1->RDA;

	msg->data[0] = (tmp>>0 ) & 0xFF;         // Get the data sent in the message
	msg->data[1] = (tmp>>8 ) & 0xFF;
	msg->data[2] = (tmp>>16) & 0xFF;
	msg->data[3] = (tmp>>24) & 0xFF;
	tmp = LPC_CAN2->RDB;
	msg->data[4] = (tmp>>0 ) & 0xFF;
	msg->data[5] = (tmp>>8 ) & 0xFF;
	msg->data[6] = (tmp>>16) & 0xFF;
	msg->data[7] = (tmp>>24) & 0xFF;

	LPC_CAN1->CMR = (1<<2); // Release the receive buffer

	can1_reset_error();

	return MESSAGE_RECEIVED;
}

/**
 * Send a message over the CAN peripheral and busy
 * wait until we are sure the message was sent.
 *
 * Only buffer 1 is used in this implementation.
 *
 * @param[in] msg The message to send over the CAN bus.
 */
void can1_send( CanMessage *msg ) 
{
	// Copy the message date from the message to the registers
	LPC_CAN1->TFI1 &= ~(0xF<<16);          // Clear the length of the message to send
	LPC_CAN1->TFI1 |= (msg->length<<16);   // Set the length of the message to send
	LPC_CAN1->TID1  = (msg->id<<0);        // Set the ID of the message to be transmitted
	LPC_CAN1->TDA1  = (msg->data[0]<<0 ) | // Set the data of the message to be transmitted
	                  (msg->data[1]<<8 ) |
	                  (msg->data[2]<<16) |
	                  (msg->data[3]<<24);
	LPC_CAN1->TDB1  = (msg->data[4]<<0 ) |
	                  (msg->data[5]<<8 ) |
	                  (msg->data[6]<<16) |
	                  (msg->data[7]<<24);

	// Request for the processor to send the message
	LPC_CAN1->CMR = (1<<4) | // This is a transmission request
	                (1<<5);  // Use transmit buffer 1

	// Wait for the message to be transmitted
	while( (LPC_CAN1->SR & (1<<2)) == 0 && (LPC_CAN1->SR & (1<<3)) == 0);

	//can1_reset_error();
}
