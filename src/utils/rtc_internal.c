#include <LPC17xx.h>

#include "rtc_internal.h"

#define RTC_EN 0ul
#define RTC_RESET 1ul
#define RTC_CALIB_EN 4ul

uint8_t days_of_month[12];

uint32_t get_doy(const uint32_t year, const uint32_t month, const uint32_t dom)
{
  uint32_t i;
  uint32_t doy = 0;
  uint8_t *months = &(days_of_month[0]);

  /********************************************************************************/
  /* Below lines added because a hard fault condition was being created due to month a nd DOM being 0
   *
   */
  if(!month || !dom)
	  return 0;
/********************************************************************************************/

  // If its a leap year and month is after feb, add one day as the lookup table has 28 days for feb
  if((year % 4) == 0 && month > 2)
  {
    ++doy;
  }

  // Get sum of days till past month
  for(i = (month - 1); i != 0; --i)
  {
    doy += *months++;
  }

  return (doy + dom - 1);
}

void rtc_init()
{
  LPC_RTC->CCR = ((1 << RTC_RESET) | (1 << RTC_CALIB_EN));
  LPC_RTC->CALIBRATION = 0;
  LPC_RTC->CCR = (1 << RTC_EN);

  if(LPC_RTC->YEAR > 2100 || LPC_RTC->YEAR < 2010)
  {
//    LPC_RTC->DOM = 1;
//    LPC_RTC->MONTH = 1;
//    LPC_RTC->YEAR = 2016;
//
//    LPC_RTC->SEC = 00;
//    LPC_RTC->MIN = 00;
//    LPC_RTC->HOUR = 00;
  }

  days_of_month[0] = 31;
  days_of_month[1] = 28;
  days_of_month[2] = 31;
  days_of_month[3] = 30;
  days_of_month[4] = 31;
  days_of_month[5] = 30;
  days_of_month[6] = 31;
  days_of_month[7] = 31;
  days_of_month[8] = 30;
  days_of_month[9] = 31;
  days_of_month[10] = 30;
  days_of_month[11] = 31;

}

void rtc_set_date(uint32_t year, uint32_t month, uint32_t day)
{
  LPC_RTC->DOM = day;
  LPC_RTC->MONTH = month;
  LPC_RTC->YEAR = year;
}

void rtc_set_time(uint32_t hour, uint32_t min, uint32_t sec)
{
  LPC_RTC->SEC = sec;
  LPC_RTC->MIN = min;
  LPC_RTC->HOUR = hour;
}

uint32_t rtc_get_now_epoch()
{
  uint32_t tm_sec = LPC_RTC->SEC;
  uint32_t tm_min = LPC_RTC->MIN;
  uint32_t tm_hour = LPC_RTC->HOUR;
  uint32_t tm_year = LPC_RTC->YEAR;
  uint32_t tm_yday = get_doy(tm_year, LPC_RTC->MONTH, LPC_RTC->DOM);

  // Formula is taken from: http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap04.html#tag_04_15
  tm_year -= 1900;
  uint32_t tm_epoch = tm_sec + tm_min*60 + tm_hour*3600 + tm_yday*86400 +
                      (tm_year-70)*31536000 + ((tm_year-69)/4)*86400 -
                      ((tm_year-1)/100)*86400 + ((tm_year+299)/400)*86400;

  return tm_epoch;
}

uint32_t rtc_get_time()
{
  return (LPC_RTC->HOUR * 60) + (LPC_RTC->MIN * 60) + LPC_RTC->SEC;
}
