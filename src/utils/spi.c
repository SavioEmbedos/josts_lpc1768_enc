/*
 * spi.c
 *
 *  Created on: 22-Apr-2017
 *      Author: acdm
 */


#include <LPC17xx.h>
#include "spi.h"

#define SSP_RNE (1<<2)
#define SSP_BSY (1<<4)

void spi_init() {
  // Power SSP0
  LPC_SC->PCONP |= (1 << 21);

  // Peripheral clock - select undivided clock for SSP0
  LPC_SC->PCLKSEL1 &= ~(3 << 10);
  LPC_SC->PCLKSEL1 |= (1 << 10);

  // Select pin functions
  //   P1.20 as SCK0 (2 at 31:30)
  LPC_PINCON->PINSEL3 |= (3 << 8);


  //   P1.23 as MISO0 (2 at 3:2)
  //   P1.24 as MOSI0 (2 at 5:4)
  LPC_PINCON->PINSEL3 |= (0x0F << 14);

//  LPC_PINCON->PINMODE3 |= (1 << 15);

  // SSP0 Control Register 0
  //   8-bit transfers (7 at 3:0)
  //   SPI (0 at 5:4)
  //   Polarity and Phase default to Mode 0
  LPC_SSP0->CR0 = 7;

  // SSP0 Prescaler
  // The SD spec requires a slow start at 200khz
  LPC_SSP0->CPSR = 18;								//min val should be 18. below that spi communication error occurs

  // SPI Control Register 1
  //   Defaults to Master
  //   Start serial communications (bit 1)
  LPC_SSP0->CR1 |= (1 << 1);
}

uint8_t spi_txrx(uint8_t tx)
{
  uint8_t rx;

  LPC_SSP0->DR = tx;
//  while ( (LPC_SSP0->SR & (SSP_BSY | SSP_RNE)) != SSP_RNE );
  while (LPC_SSP0->SR & SSP_BSY);
  rx = LPC_SSP0->DR;
  return rx;
}

