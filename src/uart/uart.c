/*
 * uart.c
 *
 *  Created on: 30-Dec-2016
 *      Author: Shivang
 */
#include <LPC17xx.h>
#include "uart.h"
#include "delay.h"

void uart_init()
{
	LPC_SC->PCONP |= (1 << 3);					//POWER ON UART0
	LPC_SC->PCLKSEL0 |= (1 << 6);				//100MHZ CLK
	LPC_PINCON->PINSEL0 |= (5 << 4);            //SETTING P0.2&P0.3 AS TXD0 & RXD0
	LPC_UART0->LCR = 0x83;                      // 8 BITS, NO PARITY, 1 STOP BIT, DLAB=1
	LPC_UART0->LCR |= (1 << 7);				    //TO ENABLE BAUD RATE REGISTERS
	LPC_UART0->DLL |= 0x8B;					    //SETTING BAUDRATE TO 9600
	LPC_UART0->DLM |= 0x02;					    //SETTING BAUDRATE TO 9600
	LPC_UART0->FCR = 0x07;                      // Enable and reset TX and RX FIFO
	LPC_UART0->LCR = 0x03;                      // 8 BITS, NO PARITY, 1 STOP BIT DLAB = 0

	LPC_GPIO0->FIODIR |= (1 << 22);				// P0.22 as output
	LPC_PINCON->PINMODE1 |= (1 << 13);			// For RS485 communication. keep the pins neither pulled up nor pulled down
	LPC_SC->PCONP |= (1 << 4);					//POWER ON UART1
	LPC_SC->PCLKSEL0 |= (1 << 8);				//100MHZ CLK
	LPC_PINCON->PINSEL0 |= (1 << 30);          	//SETTING P0.15 as TXD1
	LPC_PINCON->PINSEL1 |= (1 << 0);			//setting P0.16 as RXD1
	LPC_UART1->LCR = 0x83;                    	// 8 BITS, NO PARITY, 1 STOP BIT, DLAB=1
	LPC_UART1->LCR |= (1 << 7);					//TO ENABLE BAUD RATE REGISTERS
	LPC_UART1->DLL |= 0x8B;						//SETTING BAUDRATE TO 9600
	LPC_UART1->DLM |= 0x02;						//SETTING BAUDRATE TO 9600
	LPC_UART1->FCR = 0x07;                      // Enable and reset TX and RX FIFO
	LPC_UART1->LCR = 0x03;                 	    // 8 BITS, NO PARITY, 1 STOP BIT DLAB = 0
}

void uart_tx485(unsigned char uart485)
{
	delay_ms(3);
	LPC_GPIO0->FIOSET |= (1 << 22);
	while((LPC_UART1->LSR & (1 << 5)) == 0); 	//IS PREVIOUS DATA TRANSMITTED
	LPC_UART1->THR = uart485;					//DATA TRANSMISSION
	delay_ms(3);
}

unsigned char uart_rx485()
{
	LPC_GPIO0->FIOCLR |= ( 1 << 22);
	unsigned char uart485;
	while((LPC_UART1->LSR & (1 << 0)) == 0);    //IS DATA RECEIVED
	uart485 = LPC_UART1->RBR;					//DATA RECEPTION
	return uart485;
	//delay_ms(3);
}

void uart_tx232(unsigned char uart232)
{
	while((LPC_UART0->LSR & (1 << 5)) == 0); 	//IS PREVIOUS DATA TRANSMITTED
	LPC_UART0->THR = uart232;					//DATA TRANSMISSION
}

unsigned char uart_rx232()
{
	unsigned char uart;
	while((LPC_UART0->LSR & (1 << 0)) == 0); 	//IS DATA RECEIVED
	uart = LPC_UART0->RBR;						//DATA RECEPTION
	return uart;
}

void uart_send_str(char *string_data)
{
	while(*string_data != '\n')
	{
		uart_tx485(*string_data);
		string_data++;
	}
}
