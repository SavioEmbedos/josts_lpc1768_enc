/*
 * system_init.h
 *
 *  Created on: May 17, 2017
 *      Author: admin
 */

#ifndef SYSTEM_INIT_H_
#define SYSTEM_INIT_H_

#include<lpc17xx_i2c.h>


#define UDP_PORT 8082
#define SEND_PORT 8083
#define READ_COMMAND 0
#define WRITE_COMMAND 1
#define sensor_connected 0
#define sensor_alarm 1
#define COMM_PC_IP 0xC0A8011E
#define REQ_READ 0x40
#define REQ_WRITE 0xC0
#define SENSOR_FLAG_REG 0x00		// addressing the sensors flag register while reading the sensor statuses
#define TIMER_TICK_HZ 1000
#define UDP_DATA_PUSH_INTERVAL 1000	// 1 Sec


#define METADATA_SECTOR 0			// Metadata will use the entire sector of microSD card

//#define SECTOR_ADDR_LIMIT (((SECTOR_SIZE / 9) * 9) - 9)

#define NO_OF_PARAMETERS					10
#define TahomaBig 2
#define TahomaSmall	1
#define TahomaMega 3
#define RECT_FILL 1
#define RECT_NO_FILL 0
#define KP_X_OFFSET		23
#define KP_Y_OFFSET		6

#define MAX_NUMERIC_ENTRIES 18

#define ENC_ROTATION_PULSE_COUNT 4096


/* Keep all microSD METADATA savings address definitions here	*/
#define METADATA_SECTOR_1 0
#define MEATDATA_SECTOR_2 1
#define METADATA_SECTOR_3 2
#define METADATA_SECTOR_4 3

#define SD_MEM_PTR_LOC 	0		// 8 bytes in SD card
#define ENC_SLOPE		8		// 4 Bytes in SD card
#define ENC_OFFSET		12		// 4 Bytes in SD card
#define UNITS_ADDR		16		// 4 Bytes in SD card for units.

/*****************************************************************/
#ifndef SECTOR_SIZE
#define SECTOR_SIZE 		512
#endif
#define SD_WRT_START_SECTOR 4				// The sector from which data write starts.
#define SIZE_OF_FRAME		9				// The number of bytes a data frmae will occupy in the SD card
#define SD_WRT_FRAME_SIZE	9
#define SD_WRT_START_ADDR (SECTOR_SIZE * SD_WRT_START_SECTOR)			// The first four sectors are reserved for METADATA. So data should be stored from the sector 4 (addr. 2048)
#define SECTOR_ADDR_LIMIT (((SECTOR_SIZE / SIZE_OF_FRAME) * SIZE_OF_FRAME) - SIZE_OF_FRAME)		// The addr upto which data should be written inorder to avoid having partial data over two sectors.
#define FRAMES_PER_SECTOR (SECTOR_SIZE / SIZE_OF_FRAME)

#define NU_KEY_START_Y 45
#define NU_KEY_Y_SPACE 65
#define NU_KEY_START_X 15
#define NU_KEY_X_SPACE 115
#define NU_KEY_X_WIDTH 	100
#define NU_KEY_Y_HEIGT	50

#define SAVE_TO_SD 1		// Use with the can_send_command function. if saving to the SD card is required
#define NO_SAVE_SD 0		// Use with the can_send_command function. if no saving to the SD card is required

#define SLAVE_ADDR 80;		// 1010000

/******************* For older board	********************************/
#define SD_CS_PIN 6		// P0.6
#define TCH_CS_PIN 28
#define LCD_CS_PIN	11

/****************** For older board	************************************

#define SD_CS_PIN 6		// P0.6
#define TCH_CS_PIN 3	// P2.3
#define LCD_CS_PIN	29	// P4.29

***********************************************************************/

volatile unsigned long timer_tick;
struct netif  netif_eth0_data;
struct netif* netif_eth0 = &netif_eth0_data;

struct ip_addr my_ipaddr_data;
struct ip_addr my_netmask_data;
struct ip_addr my_gw_data;

u32_t         last_arp_time;
u32_t         last_tcpslow_time;
u32_t         last_tcpfast_time;
u32_t         last_tcp_time;
u32_t         last_push_time;
#ifdef LWIP_DHCP
static uint32_t		last_dhcpcoarse_time;
static uint32_t		last_dhcpfine_time;
#endif
u32_t         light_on, light_off;
uint32_t		recvd_UDP_data, recvd_TCP_data;
uint8_t command_type, command_RW, param_poll_num;
uint32_t device_ID, command_parameter, temp_holder_32;
uint32_t led_blink_cnt = 1;
uint64_t SD_mem_ptr;				// save the last written locations address of SD card.

/**************************************************************************************************/
/*     Testing variables
 **************************************************************************************************/
uint8_t SD_read_buffer[512];		// currently to test
uint8_t SD_write_buffer[512];		// currently to test
uint8_t SD_read_word;
uint8_t dummy_buffer[9];
/**************************************************************************************************/

volatile u32_t systick_counter;				/* counts 1ms timeTicks */
char buffer[9];
char address, command_address, data_1, data_2;
uint32_t sensor_config_len;

typedef struct {
	uint8_t command;
	uint32_t device_id;
	uint8_t data_buf[4];	// Data is stored in the array
} __attribute__((packed)) command_t;

typedef struct {
	uint32_t id;
	uint8_t flag;
} __attribute__((packed)) sensor_t;

struct com_state
{
	uint8_t state;
	uint8_t retries;
	struct tcp_pcb *pcb;
	struct pbuf *p;
};

typedef struct {
	uint16_t id;
	uint16_t index;
	uint16_t subindex;
	uint8_t data[4];
}CanStruct_t;

enum commandType{			// Commands used in the system
	NONE = 0,
	ALARM_ACK = 1,
	SENSOR_THRESHOLD,
	SENSOR_SCANTIME,
	SENSOR_CHECK,
	CHANGE_PORT,			// Command to change the ethernet communication port
	CHANGE_IPADDR,			//
	CHANGE_DATE,			// Update system date
	ADD_SENSOR,				// Add the ID of a sensor to the sensor poll list.
	CHANGE_TIME,
	SEND_LOG,
	RESET_SD_SAVE_ADDR,
	SET_CAN_BAUD
};

typedef enum
{
	COM_NONE = 0,
	COM_ACCEPTED,
	COM_RECEIVED,
	COM_CLOSING
}com_states_t;

typedef enum{
	Button_0 = 0x30,
	Button_1 = 0x31,
	Button_2 = 0x32,
	Button_3 = 0x33,
	Button_4,
	Button_5,
	Button_6,
	Button_7,
	Button_8,
	Button_9,
	Button_dot = 0x46,
	Button_remv
}button_t;

typedef struct{
	float number;
	uint32_t keypad_exit_status;	// if Ok or cancel was pressed
}keypad_status_t;

typedef struct{
	uint32_t touch_x;
	uint32_t touch_y;
}touch_t;

typedef enum{
	METERS = 1,
	FEET,
	MILLIMETERS,
	INCH
}sys_measure_units_t;

typedef struct
{
	uint8_t *buffer;
	uint8_t len_buffer;
}buffer_t;

typedef struct
{
	uint32_t sectors_sent;
	uint32_t sectors_to_send;
	uint32_t sector_send;
	uint32_t sector_send_end_value;
} sd_log_t;

#define CAN_EN		// comment to
#define NO_OF_SENSOR_POLLS 1	// for Demo of IFM RM9000 sensor
#define CAN_UDP_TX_SIZE 2*NO_OF_SENSOR_POLLS

/*********** Locations of parameters to store into the EEPROM	**********************/
#define ADDR_ENC_SLOPE 0		// enc_slope is a float
#define ADDR_ENC_OFFSET 4		// enc_offset is a float
#define ADDR_UNITS	8
#define ADDR_HOME 12
#define ADDR_FINAL_SENS 16
#define ADDR_FINAL_USER 20
#define ADDR_PASSWORD 24 // 4 bytes
// Note: If you add any more params, increase i2c_tx_buffer & i2c_rx_buffer size


command_t *command_ptr;
com_states_t com_state;
CanStruct_t can_arry[NO_OF_PARAMETERS];
uint32_t can_udp_buf[CAN_UDP_TX_SIZE];
struct pbuf *udp_buf, *tcp_buf;

/***********************************************************************************
 * Function Prototypes
 ***********************************************************************************/
void SysTick_Handler (void);
uint32_t sys_now(void);
void errorTCP(void *arg, err_t err);
err_t sent_on_tcp(void *arg, struct tcp_pcb *tpcb, uint16_t);
err_t pollTCP(void *arg, struct tcp_pcb *tpcb);
err_t sentTCP(void *arg, struct tcp_pcb *tpcb, u16_t len);
err_t recvTCP(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
void closeTCP(struct tcp_pcb *tpcb, struct com_state *com_st);
static void lwip_init(void);
//static err_t server_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err);
//static err_t client_connected(void *arg, struct tcp_pcb *pcb, err_t err);
void config_controller();
void lcd_disp_logo();
err_t write_to_tcp(struct tcp_pcb *tpcb, uint8_t *buf, uint32_t len);
void init_sensor_list();
//void SD_send_log(uint32_t epoch_time, struct tcp_pcb* pcb, struct ip_addr ip_computer);
void SD_send_log(uint64_t start_addr, uint64_t end_addr, struct tcp_pcb* pcb, struct ip_addr ip_computer);
void add_param_list();
void reset_sd_save_addr();
void eth_link_check();
void disp_keypad();
uint32_t keypad_scan();
void count_display();
void keyboard_display();
void reset_display();
void settings_units();
void disp_settings_menu_screen();
void can_send_command(uint32_t save_indicator);
//uint32_t can_send_command(uint32_t save_indicator, uint32_t timeout);
void can_operational_mode();
void i2c_init(I2C_M_SETUP_Type *i2c_trans_cfg, uint32_t clock_speed);
void super_speed_spi();
void slow_speed_spi();
void startup_delay(void);
void reset_encoder();
void send_can_msg();
void can_change_life_time();
void can_change_guard_time();

#endif /* SYSTEM_INIT_H_ */
