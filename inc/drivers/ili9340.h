/*
000000 * ili9340.h
 *
 *  Created on: 22-Apr-2017
 *      Author: acdm
 */

#pragma once

#define ILI9340_TFTWIDTH  320
#define ILI9340_TFTHEIGHT 480

#define ILI9340_NOP     0x00
#define ILI9340_SWRESET 0x01
#define ILI9340_RDDID   0x04
#define ILI9340_RDDST   0x09

#define ILI9340_SLPIN   0x10
#define ILI9340_SLPOUT  0x11
#define ILI9340_PTLON   0x12
#define ILI9340_NORON   0x13

#define ILI9340_RDMODE  0x0A
#define ILI9340_RDMADCTL  0x0B
#define ILI9340_RDPIXFMT  0x0C
#define ILI9340_RDIMGFMT  0x0A
#define ILI9340_RDSELFDIAG  0x0F

#define ILI9340_INVOFF  0x20
#define ILI9340_INVON   0x21
#define ILI9340_GAMMASET 0x26
#define ILI9340_DISPOFF 0x28
#define ILI9340_DISPON  0x29

#define ILI9340_CASET   0x2A
#define ILI9340_PASET   0x2B
#define ILI9340_RAMWR   0x2C
#define ILI9340_RAMRD   0x2E

#define ILI9340_PTLAR   0x30
#define ILI9340_MADCTL  0x36


#define ILI9340_MADCTL_MY  0x80
#define ILI9340_MADCTL_MX  0x40
#define ILI9340_MADCTL_MV  0x20
#define ILI9340_MADCTL_ML  0x10
#define ILI9340_MADCTL_RGB 0x00
#define ILI9340_MADCTL_BGR 0x08
#define ILI9340_MADCTL_MH  0x04

#define ILI9340_PIXFMT  0x3A

#define ILI9340_FRMCTR1 0xB1
#define ILI9340_FRMCTR2 0xB2
#define ILI9340_FRMCTR3 0xB3
#define ILI9340_INVCTR  0xB4
#define ILI9340_DFUNCTR 0xB6

#define ILI9340_PWCTR1  0xC0
#define ILI9340_PWCTR2  0xC1
#define ILI9340_PWCTR3  0xC2
#define ILI9340_PWCTR4  0xC3
#define ILI9340_PWCTR5  0xC4
#define ILI9340_VMCTR1  0xC5
#define ILI9340_VMCTR2  0xC7

#define ILI9340_RDID1   0xDA
#define ILI9340_RDID2   0xDB
#define ILI9340_RDID3   0xDC
#define ILI9340_RDID4   0xDD

#define ILI9340_GMCTRP1 0xE0
#define ILI9340_GMCTRN1 0xE1
/*
#define ILI9340_PWCTR6  0xFC
*/

// Color definitions
#define	ILI9340_BLACK   0x0000
#define	ILI9340_BLUE    0x001F
#define	ILI9340_RED     0xF800
#define	ILI9340_GREEN   0x07E0
#define ILI9340_CYAN    0x07FF
#define ILI9340_MAGENTA 0xF81F
#define ILI9340_YELLOW  0xFFE0
#define ILI9340_WHITE   0xFFFF
#define ILI9340_PURPLE  0x8010
#define ILI9340_ORANGE	0xFB00

#define FONT_MEGA_WIDTH	80 //67
#define FONT_MEGA_HEIGHT 120 //101

#define SET_BIT(port, bitMask) *(port) |= (bitMask)
#define CLEAR_BIT(port, bitMask) *(port) &= ~(bitMask)

#define ILI_CS_PORT				LPC_GPIO4->FIOPIN
#define ILI_CS_DIR				LPC_GPIO4->FIODIR
#define ILI_DC_PORT				LPC_GPIO2->FIOPIN
#define ILI_DC_DIR				LPC_GPIO2->FIODIR
#define ILI_RST_PORT			LPC_GPIO1->FIOPIN
#define ILI_RST_DIR				LPC_GPIO1->FIODIR

#define CS_PIN 					(1 << 29)	// P4.29
#define RST_PIN					(1 << 21)	// Actually connected to the RESET of the controller. But beacause to have usability on other boards are using the empty ARM NGK pin P0.18
#define DC_PIN 					(1 << 4)	// P2.4

#define _SB(port, pin) {port |= pin;}
#define _RB(port, pin) {port &= ~pin;}

#define CS_HI _SB(ILI_CS_PORT, CS_PIN)
#define CS_LO _RB(ILI_CS_PORT, CS_PIN)
#define RST_HI _SB(ILI_RST_PORT, RST_PIN)
#define RST_LO _RB(ILI_RST_PORT, RST_PIN)
#define DC_HI _SB(ILI_DC_PORT, DC_PIN)
#define DC_LO _RB(ILI_DC_PORT, DC_PIN)

#define BITS_PER_PIXEL 		8
#define DRAW_CHAR 			1
#define DO_NOT_DRAW_CHAR	0

void ili9340_init(void);
void ili9340_drawFastVLine(int16_t x, int16_t y, int16_t h,
 uint16_t color);
void ili9340_drawFastHLine(int16_t x, int16_t y, int16_t h, uint16_t color);
void ili9340_setRotation(uint8_t m) ;
void ili9340_drawString(uint16_t x, uint16_t y, const char *text,uint8_t font_type);
uint8_t ili9340_drawChar(uint16_t x, uint16_t y, uint8_t c, uint8_t font_type);
void ili9340_setBackColor(uint16_t col);
void ili9340_setFrontColor(uint16_t col);
void ili9340_draw_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t fill, uint16_t color);

void ili9340_setScrollStart(uint16_t start);
void ili9340_setScrollMargins(uint16_t top, uint16_t bottom);

void ili9340_draw_circle(uint16_t x, uint16_t y, uint16_t radius, uint8_t fill,uint16_t color);
void ili9340_pushColor(uint16_t color);
void ili9340_clear_display(uint16_t color);
void ili9340_draw_Hline(uint16_t x, uint16_t y, uint16_t length, uint16_t color);
void ili9340_draw_Vline(uint16_t x, uint16_t y, uint16_t heigth, uint16_t color);
void ili9340_display_printf(int x,int y, uint8_t font_type, const char *p, ...);
void ili9340_display_printf_num(int x,int y,uint8_t font_type, int32_t comp_num, const char *p, ...);
void ili9340_draw_bitmap(uint32_t x_start, uint32_t y_start, const unsigned char* image, uint16_t color);
void ili9340_draw_color_bitmap();
uint16_t rgb565_converter(uint8_t red, uint8_t green, uint8_t blue);

uint16_t ili9340_width(void);
uint16_t ili9340_height(void);


#ifdef __cplusplus
}
#endif
