/*
 * xpt2046.h
 *
 *  Created on: 22-Apr-2017
 *      Author: acdm
 */

#ifndef INC_XPT2046_H_
#define INC_XPT2046_H_
#include <stdint.h>

int16_t xpt2046_x, xpt2046_y;
int16_t data[6];
void xpt2046_init();
uint8_t xpt2046_get_data();

#endif /* INC_XPT2046_H_ */
