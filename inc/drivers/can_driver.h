#include <stdint.h>

#ifndef CAN_H__
#define CAN_H__

typedef struct {
	uint16_t id;
	uint8_t length;
	uint8_t data[8];
} CanMessage;

typedef enum {
	NO_MESSAGE_RECEIVED = 0,
	MESSAGE_RECEIVED    = 1,
	ZERO_DATA_RECVD		= 2
} CanReceiveStatus;

void can2_init(uint8_t baud_rate);
void can2_deinit( void );
CanReceiveStatus can2_receive( CanMessage *msg );
uint8_t can2_send( CanMessage *msg, uint8_t is_rtr);

void can1_init( void );
void can1_deinit( void );
CanReceiveStatus can1_receive( CanMessage *msg );
void can1_send( CanMessage *msg);

#endif
