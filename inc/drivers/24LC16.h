#define I2C0        	0
#define I2C1			1
#define I2C2			2

/*Set 0 for ports 0.0 and 0.1 ---- Set 1 for ports 0.19 and 0.20*/
#define selected_port	0

#define Block0			0b10100000
#define Block1			0b10100010
#define Block2			0b10100100
#define Block3			0b10100110
#define Block4			0b10101000
#define Block5			0b10101010
#define Block6			0b10101100
#define Block7			0b10101110

uint8_t i2c_address_read;
uint8_t i2c_address_write;

void EEPROMInit(uint32_t peripheral);
void EEPROMSetBlock(uint8_t blocknr);
void EEPROMWrite(uint32_t peripheral,uint32_t memory_address, uint8_t value);
uint8_t EEPROMRead(uint32_t peripheral,uint32_t memory_address);
