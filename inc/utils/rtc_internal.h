#pragma once

#include <stdint.h>

void rtc_init();
uint32_t rtc_get_now_epoch();
void rtc_set_date(uint32_t year, uint32_t month, uint32_t day);
void rtc_set_time(uint32_t hour, uint32_t min, uint32_t sec);
