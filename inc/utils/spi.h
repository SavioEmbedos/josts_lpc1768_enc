/*
 * spi.h
 *
 *  Created on: 22-Apr-2017
 *      Author: acdm
 */

#ifndef INC_SPI_H_
#define INC_SPI_H_
#include <stdint.h>

void spi_init();
uint8_t spi_txrx(uint8_t tx);

#endif /* INC_SPI_H_ */
